// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "SwitchLevelValues.h"
#include "ArcanumCloneGameMode.generated.h"

UCLASS(minimalapi)
class AArcanumCloneGameMode : public AGameMode
{
	GENERATED_BODY()

		virtual void BeginPlay() override;

public:
	AArcanumCloneGameMode();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UAudioComponent* MainMusic;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UAudioComponent* BattleMusic;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		USoundBase* MainMusicTheme;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		USoundBase* BattleMusicTheme;

	UPROPERTY(BlueprintReadOnly)
		bool bAttackMusicCurrentlyRunning;

	UPROPERTY(BlueprintReadOnly)
		bool bMainMusicCurrentlyRunning;

protected:

	/**
	* This will be the main HUD
	*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "HUD", Meta = (BlueprintProtected = "true"))
		TSubclassOf<class UUserWidget> PlayerHUDClass;

	UPROPERTY()
	class UUserWidget* currentWidget;

};



