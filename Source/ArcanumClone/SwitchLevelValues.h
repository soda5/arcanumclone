// Mischa Ahi / Philip Gandy @ 2016

#pragma once

#include "Engine/GameInstance.h"
#include "SwitchLevelValues.generated.h"

UCLASS()
class ARCANUMCLONE_API USwitchLevelValues : public UGameInstance
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintReadWrite)
		/**
		* true = the level was changed 
		* false = this is a loaded savegame
		*/
		bool bChangedLevel;

	UPROPERTY(BlueprintReadWrite)
		FString Slot;

	UPROPERTY(BlueprintReadWrite)
		FString TargetLevel;

};