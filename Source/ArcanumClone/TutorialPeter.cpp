// Mischa Ahi / Philip Gandy @ 2016

#include "ArcanumClone.h"
#include "TutorialPeter.h"
#include "ArcanumCloneCharacter.h"

ATutorialPeter::ATutorialPeter()
{
	silenceCounter = 0;
	dialoguePosition = 1;
}

void ATutorialPeter::SetText()
{
	dialogueOptions.Empty();


	switch (dialoguePosition)
	{
	case 1:
		dialogueSentence = "Hi! \nI am TutorialPeter, I am here to tell you the basics of the Game :)";
		dialogueOptions.Emplace("Hi TutorialPeter. What should we two beauties do today?");
		dialogueOptions.Emplace("Oh. Nice to meet you Peter.");
		dialogueOptions.Emplace("Really?? A Tutorial in 2017?");
		dialogueOptions.Emplace("...");
		break;
	case 2:
		dialogueSentence = "Like I said I am gonna teach you how this game works. And you only have to follow my suggestions.";
		dialogueOptions.Emplace("Yes Sir, Sir yes.");
		dialogueOptions.Emplace("Sure.");
		dialogueOptions.Emplace("...");
		break;
	case 3:
		dialogueSentence = "Good Response my friend. You should step in the big hall behind me and talk to my clone there.";
		dialogueOptions.Emplace("Okay Sir.");
		dialogueOptions.Emplace("On my way.");
		break;
	case 4:
		dialogueSentence = "Nice to meet you too. First of, my Eyesight is pretty bad, so can you tell me are you a boy or a girl?";
		dialogueOptions.Emplace("I am a Boy.");
		dialogueOptions.Emplace("I am a real Man!");
		dialogueOptions.Emplace("I am a girl.");
		dialogueOptions.Emplace("...");
		break;
	case 5:
		dialogueSentence = "Well doesn't really matter anyways, since in this World there are no differences between Man and Woman. You should step in the big hall behind me and talk to my clone there.";
		dialogueOptions.Emplace("Okay.");
		dialogueOptions.Emplace("On my way.");
		dialogueOptions.Emplace("...");
		break;
	case 6:
		dialogueSentence = "Of course you are. Just proceed to my clone inside.";
		dialogueOptions.Emplace("... fine.");
		break;
	case 7:
		dialogueSentence = "Yes! A real Tutorial in 2017, and because you doubted my importance let me tell you about the Impact of a good Tutorial.";
		dialogueOptions.Emplace("I'm sorry, I'm sorry.");
		dialogueOptions.Emplace("Oh God please don't.");
		break;
	case 8:
		dialogueSentence = "Since you are sorry I will forget your ill responses.";
		dialogueOptions.Emplace("...");
		dialogueOptions.Emplace("Thanks.");
		dialogueOptions.Emplace("...thanks.");
		break;
	case 9:
		dialogueSentence = "You see those two guys over there? they are refusing to pay there Debt to me. \n Click on the 'Combat'-Button on your Screen to activate Combat mode to beat them up";
		dialogueOptions.Emplace("Okay sir.");
		dialogueOptions.Emplace("But I don't want to");
		dialogueOptions.Emplace("...");
		break;
	case 10:
		dialogueSentence = "Maybe you should arrange your Skillpoints first. you can do that in the Charactermenu. Click on the C button on the top bar.";
		dialogueOptions.Emplace("Okay, Thanks.");
		dialogueOptions.Emplace("...");
		break;
	case 11:
		dialogueSentence = "Fine then you don't have to. Meet me in the Room on our right side.";
		dialogueOptions.Emplace("Okay, see you there.");
		break;
	case 12:
		dialogueSentence = "I am not sure what you did to them but that doesn't matter now. Meet me in the room on our right side.";
		dialogueOptions.Emplace("see you there.");
		dialogueOptions.Emplace("...");
		break;
	case 13:
		dialogueSentence = "It seems these goons are blocking our way. you should use Magic to Push them out of the way. either Air 2 or Air 3 should do the Trick.";
		dialogueOptions.Emplace("But I don't have any mana left.");
		dialogueOptions.Emplace("Consider it Done.");
		dialogueOptions.Emplace("...");
		break;
	case 14:
		dialogueSentence = "then you have to fight your way through.";
		dialogueOptions.Emplace("Okay.");
		break;
	case 15:
		dialogueSentence = "You look kind of poor why don't you take this Equipment right next to me?";
		dialogueOptions.Emplace("As you Wish master.");
		dialogueOptions.Emplace("Lovely Loot is lovely. I guess I take it.");
		dialogueOptions.Emplace("...");
		break;
	case 16:
		dialogueSentence = "When you open your Inventorymenu you can equip and unequip Items by clicking on them, some items have also other uses like healing you or giving you mana.";
		dialogueOptions.Emplace("thanks for the Info");
		dialogueOptions.Emplace("...");
		break;
	case 17:
		dialogueSentence = "Well this is our last meeting. I heard some crazy Dude attacked at least two Guys of Reteps crew and than went on to enter their base. \nHe even stole Reteps favourite set of Equipment. Unbelieveable right?";
		dialogueOptions.Emplace("Wait ... what?");
		dialogueOptions.Emplace("But you told me to do so!");
		dialogueOptions.Emplace("...");
		break;
	case 18:
		dialogueSentence = "What are you talking about? I would never do something like that!";
		dialogueOptions.Emplace("...");
		dialogueOptions.Emplace("Screw you!");
		break;
	case 19:
		dialogueSentence = "I guess Retep is teleporting you to him right now. Soooooooo...\nBye Bye! :)";
		dialogueOptions.Emplace("...");
		break;
	case 20:
		dialogueSentence = "It's dangerous to go alone Take this";
		dialogueOptions.Emplace("...");
		break;
	case 21:
		dialogueSentence = "Without a Tutorial the new Player Experience could be unbelievable bad and therefore it could leed to a relativly short lived game because of the lack of a core Fanbase. \nFurthermore a good Tutorial is enjoyable and will be played without regrets but I don't want to \n trap you now inside of a stupid joke so you can just enjoy the boss fight.";
		dialogueOptions.Emplace("...");
		break;
	default:
		dialogueSentence = "ERROR!ERROR!ERROR!";
		break;
	}
	numberOfAnswers = dialogueOptions.Num();
}

int ATutorialPeter::ReturnNextDialogPosition(int choosenAnswer)
{
	if (!choosenAnswer)
		return 1;
	AArcanumCloneCharacter* playerCharacter = (AArcanumCloneCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	switch (dialoguePosition)
	{
	case 1:
		if (choosenAnswer == 1)
			dialoguePosition = 2;
		else if (choosenAnswer == 3)
			dialoguePosition = 7;
		else
		{
			if (choosenAnswer == 4)
				silenceCounter++;
			dialoguePosition = 2;
		}
		break;
	case 2:
		if (choosenAnswer == 3)
		{
			silenceCounter++;
			dialoguePosition = 4;
		}
		else
			dialoguePosition = 3;
		break;
	case 3:
		bEndDialogHere = true;
		TeleportTo(FVector(2100, 200, 200), FRotator(0, 180, 0), true, false);
		dialoguePosition = 9;
		break;
	case 4:
		if (choosenAnswer == 1)
		{
			playerCharacter->SetSex("Boy");
			dialoguePosition = 5;
		}
		if (choosenAnswer == 2)
		{
			playerCharacter->SetSex("'real Man!'");
			dialoguePosition = 6;
		}
		if (choosenAnswer == 3)
		{
			playerCharacter->SetSex("Girl");
			dialoguePosition = 5;
		}
		if (choosenAnswer == 4)
		{
			silenceCounter++;
			dialoguePosition = 5;
		}
		break;
	case 5:
		bEndDialogHere = true;
		if (choosenAnswer == 3)
			silenceCounter++;
		TeleportTo(FVector(2100, 200, 200), FRotator(0, 180, 0), true, false);
		dialoguePosition = 9;
		break;
	case 6:
		bEndDialogHere = true;
		TeleportTo(FVector(2100, 200, 200), FRotator(0, 180, 0), true, false);
		dialoguePosition = 9;
		break;
	case 7:
		if (choosenAnswer == 1)
			dialoguePosition = 8;
		else
		{
			dialoguePosition = 21;
		}
		break;
	case 8:
		dialoguePosition = 4;
		break;
	case 9:
		bEndDialogHere = false;
		if (choosenAnswer == 2)
			dialoguePosition = 11;
		else
			dialoguePosition = 10;
		if (choosenAnswer == 3)
			silenceCounter++;
		break;
	case 10:
		bEndDialogHere = true;
		if (choosenAnswer == 2)
			silenceCounter++;
		dialoguePosition = 12;
		break;
	case 11:
		TeleportTo(FVector(1900, 2600, 200), FRotator(0, 270, 0), true, false);
		bEndDialogHere = true;
		dialoguePosition = 13;
		break;
	case 12:
		if (choosenAnswer == 2)
			silenceCounter++;
		TeleportTo(FVector(1900, 2600, 200), FRotator(0, 270, 0), true, false);
		dialoguePosition = 13;
		break;
	case 13:
		if (choosenAnswer == 1)
		{
			dialoguePosition = 14;
			bEndDialogHere = false;
		}
		else
		{
			dialoguePosition = 15;
			TeleportTo(FVector(2050, 3800, 200), FRotator(0, 270, 0), true, false);
		}
		if (choosenAnswer == 3)
			silenceCounter++;
		break;
	case 14:
		bEndDialogHere = true;
		TeleportTo(FVector(2050, 3800, 200), FRotator(0, 270, 0), true, false);
		dialoguePosition = 15;
		break;
	case 15:
		if (choosenAnswer == 3)
			silenceCounter++;
		bEndDialogHere = false;
		dialoguePosition = 16;
		break;
	case 16:
		if (choosenAnswer == 2)
			silenceCounter++;
		bEndDialogHere = true;
		dialoguePosition = 17;
		break;
	case 17:
		bEndDialogHere = false;
		if (choosenAnswer == 1)
			dialoguePosition = 17;
		else
			dialoguePosition = 18;
		if (choosenAnswer == 3)
			silenceCounter++;
		break;
	case 18:
		if (choosenAnswer == 1)
			silenceCounter++;
		dialoguePosition = 19;
		break;
	case 19:
		bEndDialogHere = true;
		dialoguePosition = 1;
		playerCharacter->TeleportTo(FVector(0, -8740, 100), FRotator(0, 0, 0));
		break;
	case 20:
		bEndDialogHere = true;
		dialoguePosition = 1;
		playerCharacter->TeleportTo(FVector(-800, 1780, 100), FRotator(0, 0, 0));
		break;
	case 21:
		bEndDialogHere = true;
		dialoguePosition = 1;
		playerCharacter->TeleportTo(FVector(0, -8740, 100), FRotator(0, 0, 0));
		break;
	default:
		dialoguePosition = 1;
		silenceCounter = 0;
		break;
	}
	if (silenceCounter == 12)
		dialoguePosition = 20;
	return dialoguePosition;
}