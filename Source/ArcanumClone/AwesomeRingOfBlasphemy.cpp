// Mischa Ahi / Philip Gandy @ 2016

#include "ArcanumClone.h"
#include "AwesomeRingOfBlasphemy.h"

AAwesomeRingOfBlasphemy::AAwesomeRingOfBlasphemy()
{
	Name = "AwesomeRingOfBlasphemy";

	mana = 100;
	healthPoints = 100;
	armorvalue = 10;
	damageModifier = 1.33;

	ToolTip = "Awesome Ring of Blasphemy \n \nIt is said that the owner of this Ring will Increase his Power \nby not caring about other people's opinions and use \nthe power of Disbelief to Destroy all his Enemies \n \nDamage + 133 % \nArmor + 10 \nMana+400 \nHealth+400";
}

void AAwesomeRingOfBlasphemy::GiveStats()
{
	Super::GiveStats();
	AArcanumCloneCharacter* playerCharacter = (AArcanumCloneCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	playerCharacter->IncreaseManaGrowth(mana);
	playerCharacter->IncreaseHealthGrowth(healthPoints);
}

void AAwesomeRingOfBlasphemy::RemoveStats()
{
	Super::RemoveStats();
	AArcanumCloneCharacter* playerCharacter = (AArcanumCloneCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	playerCharacter->IncreaseManaGrowth(-mana);
	playerCharacter->IncreaseHealthGrowth(-healthPoints);
}