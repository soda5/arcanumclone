// Mischa Ahi / Philip Gandy @ 2016

#include "ArcanumClone.h"
#include "Merchant.h"
#include "AmmunitionPickUp.h"
#include "MoneyPickUp.h"
#include "BootsItem.h"
#include "BreastPlateItem.h"
#include "HelmetItem.h"
#include "PantsItem.h"
#include "RingItem.h"
#include "ShieldItem.h"
#include "WeaponItem.h"
#include "HealthPotion.h"

AMerchant::AMerchant()
{
}

void AMerchant::BeginPlay()
{
	UWorld* world = GetWorld();

	AAmmunitionPickUp* ammo = world->SpawnActor<AAmmunitionPickUp>(AmmoBP, GetActorLocation(), FRotator(0, 0, 0));
	ABootsItem* boots = world->SpawnActor<ABootsItem>(BootsBP, GetActorLocation(), FRotator(0, 0, 0));
	AWeaponItem* weapon = world->SpawnActor<AWeaponItem>(WeaponBP, GetActorLocation(), FRotator(0, 0, 0));
	AHelmetItem* helmet = world->SpawnActor<AHelmetItem>(HelmetBP, GetActorLocation(), FRotator(0, 0, 0));
	AShieldItem* shield = world->SpawnActor<AShieldItem>(ShieldBP, GetActorLocation(), FRotator(0, 0, 0));
	AHealthPotion* hpPotion = world->SpawnActor<AHealthPotion>(HPPotionBP, GetActorLocation(), FRotator(0, 0, 0));
	AHealthPotion* hpPotion2 = world->SpawnActor<AHealthPotion>(HPPotionBP, GetActorLocation(), FRotator(0, 0, 0));
	AHealthPotion* hpPotion3 = world->SpawnActor<AHealthPotion>(HPPotionBP, GetActorLocation(), FRotator(0, 0, 0));
	//APantsItem* pants = world->SpawnActor<APantsItem>(PantsBP, GetActorLocation(), FRotator(0, 0, 0));
	//ARingItem* ring = world->SpawnActor<ARingItem>(RingBP, GetActorLocation(), FRotator(0, 0, 0));
	//ABreastPlateItem* breastPlate = world->SpawnActor<ABreastPlateItem>(BreastPlateBP, GetActorLocation(), FRotator(0, 0, 0));

	Deactivate(ammo);
	Deactivate(boots);
	Deactivate(weapon);
	Deactivate(helmet);
	Deactivate(shield);
	Deactivate(hpPotion);
	Deactivate(hpPotion2);
	Deactivate(hpPotion3);

	inventory.Add(ammo);
	inventory.Add(boots);
	inventory.Add(weapon);
	inventory.Add(helmet);
	inventory.Add(shield);
	inventory.Add(hpPotion);
	inventory.Add(hpPotion2);
	inventory.Add(hpPotion3);
}

void AMerchant::Deactivate(APickUp* pickUp)
{
	pickUp->Mesh->SetSimulatePhysics(false);
	pickUp->SetActorEnableCollision(false);
	pickUp->SetActorHiddenInGame(true);
}