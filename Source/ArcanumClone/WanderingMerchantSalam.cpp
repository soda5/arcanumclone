// Mischa Ahi / Philip Gandy @ 2016

#include "ArcanumClone.h"
#include "WanderingMerchantSalam.h"


AWanderingMerchantSalam::AWanderingMerchantSalam()
{
	dialoguePosition = 1;
}

void AWanderingMerchantSalam::SetText()
{
	dialogueOptions.Empty();

	switch (dialoguePosition)
	{
	case 1:
		dialogueSentence = TEXT("What can I do for you my friend?");
		dialogueOptions.Emplace(TEXT("Show me what you sell."));
		dialogueOptions.Emplace(TEXT("Tell me about yourself."));
		dialogueOptions.Emplace(TEXT("You know this place?"));
		dialogueOptions.Emplace(TEXT("I don't need anything right now."));
		if (playerCharacter->bStartedMerchantsPendant)
			dialogueOptions.Emplace(TEXT("I heard you got a special Pendant."));
		break;
	case 2:
		dialogueSentence = TEXT("I am Salam. Nothing more but a wandering Merchant, trying to survive in these harsh times. So if you wouldn't mind I don't really want to talk that much about myself. I rather here about your needs and we see if I have what you need.");
		dialogueOptions.Emplace(TEXT("Okay then show me what you sell."));
		if (playerCharacter->GetCharisma() > 12)
			dialogueOptions.Emplace(TEXT("Everybody has done something special. I just like to here tales of the people I meet."));
		if (playerCharacter->bStartedMerchantsPendant)
			dialogueOptions.Emplace(TEXT("Indeed I look for something in your possesion."));
		dialogueOptions.Emplace(TEXT("Nevermind I don't need anything right now."));
		break;
	case 3:
		dialogueSentence = TEXT("I just arrived here. Looking for shelter from the Goblins.");
		if (playerCharacter->bTalkedWithThePriest)
			dialogueOptions.Emplace(TEXT("How did you get here. I heard the Forest is of limits."));
		dialogueOptions.Emplace(TEXT("Goblins?"));
		dialogueOptions.Emplace(TEXT("How wonderful. I am looking for a way out of this Village."));
		break;
	case 4:
		dialogueSentence = TEXT("You must have spoken with the Priest. I do have the Pendant the Priest wants. actually I used to have two. But since I lost one the Value of this one I still have just grew. I would be fine with 200 Gold.");
		dialogueOptions.Emplace(TEXT("That is too much I can't pay you."));
		dialogueOptions.Emplace(TEXT("Fine. Here take it."));
		if (playerCharacter->GetCharisma() > 13)
			dialogueOptions.Emplace(TEXT("I don't think any Objekt should be that expensive in your Situation. I mean how will you get out of here?"));
		if (playerCharacter->GetBeauty() > 11)
			dialogueOptions.Emplace(TEXT("I might give you something 'special' for a little discount."));
		dialogueOptions.Emplace(TEXT("What about the other Pendant. The one you lost."));
		break;
	case 5:
		dialogueSentence = TEXT("I really don't know if I can trust you.");
		dialogueOptions.Emplace(TEXT("Well I guess you don't have to tell me. I still wanna talk with you though."));
		if (playerCharacter->GetIntelligence() > 11)
			dialogueOptions.Emplace(TEXT("That seems reasonable, but you never no who might be able to help you in the Future."));
		else
			dialogueOptions.Emplace(TEXT("Ahh. Come On! I won't tell anybody. I promise."));
		if (playerCharacter->GetCharisma() > 15)
			dialogueOptions.Emplace(TEXT("Who can you trust in this wild Time. Doesn't matter if royal guard or bum, all try to sell you in the End. But shouldn't we Travelers hold together since no one else does?"));
		else
			dialogueOptions.Emplace(TEXT("Do I look shifty to you?! I don't look shifty right?"));
		break;
	case 6:
		dialogueSentence = TEXT("That is true. You see there is a little Cave to the east of us. There used to be a small passage there. But when I got through it broke down behind me. Maybe a Magician could get the rocks out of the way.");
		dialogueOptions.Emplace(TEXT("I will see what I can do."));
		dialogueOptions.Emplace(TEXT("Thanks for the Info anyways."));
		dialogueOptions.Emplace(TEXT("Show me your Wares, I might need something."));
		break;
	case 7:
		dialogueSentence = TEXT("You didn't notice? They are everywhere.");
		dialogueOptions.Emplace(TEXT("Oh. I didn't know."));
		dialogueOptions.Emplace(TEXT("Thanks for the Information."));
		break;
	case 8:
		dialogueSentence = TEXT("I really can't help you with that. I'm sorry.");
		dialogueOptions.Emplace(TEXT("Seems like I am out of luck. There are other things I want to know."));
		dialogueOptions.Emplace(TEXT("Then I have all the Informations I need. Until next time."));
		if (playerCharacter->GetCharisma() > 11)
			dialogueOptions.Emplace(TEXT("Are you really sure you didn't forget something?"));
		break;
	case 9:
		dialogueSentence = TEXT("Okay, fine. Since you are a special Customer I can give it to you for 150 gold");
		dialogueOptions.Emplace(TEXT("I take it."));
		dialogueOptions.Emplace(TEXT("That is still to much for me."));
		dialogueOptions.Emplace(TEXT("What about the other Pendant. The one you lost."));
		break;
	case 10:
		dialogueSentence = TEXT("No I won't tell you.");
		dialogueOptions.Emplace(TEXT("Fine."));
		break;
	case 11:
		dialogueSentence = TEXT("If you can find it it's yours. I must have lost it when the Goblins attacked me in the Cave to the east.");
		dialogueOptions.Emplace(TEXT("I will see what I can do."));
		dialogueOptions.Emplace(TEXT("Show me what you sell. I might need it."));
		break;
	default:
		dialogueSentence = TEXT("ERROR!ERROR!ERROR!");
		dialogueOptions.Emplace(TEXT("RESET DIALOGTREE!"));
		break;
	}
	numberOfAnswers = dialogueOptions.Num();
}

int AWanderingMerchantSalam::ReturnNextDialogPosition(int choosenAnswer)
{
	if (!choosenAnswer)
		dialoguePosition = 1;
	switch (dialoguePosition)
	{
	case 1:
		bEndDialogHere = false;
		if (choosenAnswer == 1)
		{
			bEndDialogHere = true;
			OpenShop();
		}
		else if (choosenAnswer == 2)
			dialoguePosition = 2;
		else if (choosenAnswer == 3)
			dialoguePosition = 3;
		else if (choosenAnswer == 4)
			bEndDialogHere = true;
		else if (choosenAnswer == 5)
			dialoguePosition = 4;
		else
			dialoguePosition = 1;
		break;
	case 2:
		if (choosenAnswer == 1)
		{
			bEndDialogHere = true;
			dialoguePosition = 1;
			OpenShop();
		}
		else if (choosenAnswer == 2)
		{
			if (playerCharacter->GetCharisma() > 12)
				dialoguePosition = 5;
			else if (playerCharacter->bStartedMerchantsPendant)
				dialoguePosition = 4;
			else
			{
				bEndDialogHere = true;
				dialoguePosition = 1;
			}
		}
		else if (choosenAnswer == 3)
		{
			if (playerCharacter->GetCharisma() > 12 && playerCharacter->bStartedMerchantsPendant)
				dialoguePosition = 4;
			else
			{
				bEndDialogHere = true;
				dialoguePosition = 1;
			}
		}
		else if (choosenAnswer == 4)
		{
			bEndDialogHere = true;
			dialoguePosition = 1;
		}
		break;
	case 3:
		if (playerCharacter->bTalkedWithThePriest)
			choosenAnswer -= 1;
		if (choosenAnswer == 0)
			dialoguePosition = 6;
		else if (choosenAnswer == 1)
			dialoguePosition = 7;
		else if (choosenAnswer == 2)
			dialoguePosition = 8;
		break;
	case 4:
		if (choosenAnswer == 1)
			dialoguePosition = 1;
		else if (choosenAnswer == 2)
		{
			if (playerCharacter->GetMoney() >= 200)
			{
				playerCharacter->SetMoney(playerCharacter->GetMoney() - 200);
				world->SpawnActor<AMerchantsPendant>(MerchantsPendantBP, GetActorLocation() + FVector(0, 100, 0), FRotator(0, 0, 0));
			}
			bEndDialogHere = true;
			dialoguePosition = 1;
		}
		else if (choosenAnswer == 3)
		{
			if (playerCharacter->GetCharisma() > 13 || playerCharacter->GetBeauty() > 11)
				dialoguePosition = 9;
			else
				dialoguePosition = 11;
		}
		else if (choosenAnswer == 4)
		{
			if (playerCharacter->GetCharisma() > 13 && playerCharacter->GetBeauty() > 11)
				dialoguePosition = 9;
			else
				dialoguePosition = 11;
		}
		else if (choosenAnswer == 5)
			dialoguePosition = 11;
		break;
	case 5:
		if (choosenAnswer == 1)
			dialoguePosition = 1;
		else
		{
			if (playerCharacter->GetIntelligence() > 11 || playerCharacter->GetCharisma() > 15)
				dialoguePosition = 6;
			else
				dialoguePosition = 10;
		}
		break;
	case 6:
		bEndDialogHere = true;
		dialoguePosition = 1;
		if (choosenAnswer == 3)
			OpenShop();
		break;
	case 7:
		bEndDialogHere = true;
		dialoguePosition = 1;
		break;
	case 8:
		if (choosenAnswer == 2)
			bEndDialogHere = true;
		dialoguePosition = 1;
		if (choosenAnswer == 3)
			dialoguePosition = 5;
		break;
	case 9:
		if (choosenAnswer == 1)
		{
			if (playerCharacter->GetMoney() >= 150)
			{
				playerCharacter->SetMoney(playerCharacter->GetMoney() - 150);
				world->SpawnActor<AMerchantsPendant>(MerchantsPendantBP, GetActorLocation() + FVector(0, 100, 0), FRotator(0, 0, 0));
			}
			dialoguePosition = 1;
		}
		else if (choosenAnswer == 2)
			dialoguePosition = 1;
		else
			dialoguePosition = 11;

		break;
	case 10:
		dialoguePosition = 1;
		break;
	case 11:
		bEndDialogHere = true;
		dialoguePosition = 1;
		if (choosenAnswer == 2)
			OpenShop();
		break;
	default:
		dialoguePosition = 1;
		break;
	}
	//world->SpawnActor<AMerchantsPendant>(MerchantsPendantBP, GetActorLocation() + FVector(0, 100, 0), FRotator(0, 0, 0));
	return dialoguePosition;
}

void AWanderingMerchantSalam::GetReferences()
{
	playerCharacter = (AArcanumCloneCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	world = GetWorld();
}