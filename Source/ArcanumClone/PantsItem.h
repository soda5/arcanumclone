// Mischa Ahi / Philip Gandy @ 2016

#pragma once

#include "GearItem.h"
#include "PantsItem.generated.h"

UCLASS()
class ARCANUMCLONE_API APantsItem : public AGearItem
{
	GENERATED_BODY()
	
public:
	APantsItem();

	void Use() override;

	void GiveStats() override;

	void RemoveStats() override;

protected:
	
	int armorvalue;

};