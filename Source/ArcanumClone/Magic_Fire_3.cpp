// Mischa Ahi / Philip Gandy @ 2016

#include "ArcanumClone.h"
#include "Magic_Fire_3.h"
#include "BaseCharacter.h"

UMagic_Fire_3::UMagic_Fire_3()
{
	ManaCosts = 20;
	bIsInstant = true;
}

void UMagic_Fire_3::Costs(UWorld* world)
{
	Super::Costs(world);

	if (world)
	{
		APlayerController* controller = world->GetFirstPlayerController();

		if (controller)
		{
			ACharacter* character = controller->GetCharacter();
			if (character)
			{
				ABaseCharacter* playerCharacter = dynamic_cast<ABaseCharacter*>(character);
				if (playerCharacter->GetCurrentMana() >= ManaCosts)
				{
					playerCharacter->SetCurrentMana(-ManaCosts);
					bManaCostsHasBeenPayed = true;
				}
				else
					bManaCostsHasBeenPayed = false;
			}
		}
	}
}

void UMagic_Fire_3::ActivateSpell(ABaseCharacter* target, UWorld* world)
{
	Super::ActivateSpell(target, world);

	if (world)
	{
		APlayerController* controller = world->GetFirstPlayerController();

		if (controller)
		{
			ACharacter* character = controller->GetCharacter();
			if (character)
			{
				ABaseCharacter* playerCharacter = dynamic_cast<ABaseCharacter*>(character);

				if (bManaCostsHasBeenPayed)
				{
					for (TActorIterator<ABaseCharacter> ActorItr(world); ActorItr; ++ActorItr)
					{
						ABaseCharacter* Character = *ActorItr;

						FVector playerPosition = playerCharacter->GetActorLocation();
						FVector targetPosition = Character->GetActorLocation();

						FVector vector = playerPosition - targetPosition;

						float length = vector.Dist(playerPosition, targetPosition);

						if (length <= 500)
						{
							if (!Character->bPlayer)
							{
								Character->AddToCurrentHealth(-playerCharacter->GetIntelligence() * 2);
							}
						}
					}
				}
			}
		}
	}
}