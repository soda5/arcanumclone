// Mischa Ahi / Philip Gandy @ 2016

#include "ArcanumClone.h"
#include "HealthPotion.h"

AHealthPotion::AHealthPotion()
{
	Name = TEXT("HealthPotion");

	healamount = 25;
	price = 30;

	ToolTip = TEXT("Heals the Player for a maximum of 25 Healthpoints");
	
}

void AHealthPotion::Use()
{
	AArcanumCloneCharacter* character = (AArcanumCloneCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	if (character->GetMaxHealth() - character->GetCurrentHealth() < healamount)
		character->SetCurrentHealth(character->GetMaxHealth());
	else
		character->SetCurrentHealth(character->GetCurrentHealth() + healamount);
	character->RemoveFromInventory(this);
}
