// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "ArcanumClone.h"
#include "ArcanumCloneGameMode.h"
#include "ArcanumClonePlayerController.h"
#include "ArcanumCloneCharacter.h"
#include "Blueprint/UserWidget.h"

void AArcanumCloneGameMode::BeginPlay()
{
	Super::BeginPlay();

	AArcanumCloneCharacter* ArcanumCloneCharacter = Cast<AArcanumCloneCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));

	if (PlayerHUDClass != nullptr)
	{
		currentWidget = CreateWidget<UUserWidget>(GetWorld(), PlayerHUDClass);

		if (currentWidget != nullptr)
		{
			currentWidget->AddToViewport();
		}
	}
}

AArcanumCloneGameMode::AArcanumCloneGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AArcanumClonePlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	MainMusic = CreateDefaultSubobject<UAudioComponent>(TEXT("MainMusic"));
	MainMusic->SetupAttachment(RootComponent);
	MainMusic->SetSound(MainMusicTheme);

	BattleMusic = CreateDefaultSubobject<UAudioComponent>(TEXT("BattleMusic"));
	BattleMusic->SetupAttachment(RootComponent);
	BattleMusic->SetSound(BattleMusicTheme);
}