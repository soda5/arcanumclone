// Mischa Ahi @ 2016

#include "ArcanumClone.h"
#include "PickUp.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
APickUp::APickUp()
{
	PrimaryActorTick.bCanEverTick = true;

	//Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	RootComponent = Mesh;
	//Mesh->AttachToComponent(Root, FAttachmentTransformRules::KeepRelativeTransform);
	/*Mesh->SetMobility(EComponentMobility::Movable);
	Mesh->SetSimulatePhysics(true);
	Mesh->WakeRigidBody();
	Mesh->SetMassOverrideInKg(NAME_None, 100, true);
	Mesh->SetEnableGravity(true);*/
	Mesh->SetEnableGravity(true);


	ConstructorHelpers::FObjectFinder<UTexture2D> Texture(TEXT("Texture2D'/Game/Textures/Unknown_Item.Unknown_Item'"));

	Icon = Texture.Object;
	
	ToolTip = "Greatsword of Astorah\n Damage 12-42 \n +2% Critical Hitchance";


	PickUpRange = 200;
	InventorySpace = 1;
	bShallAppearInInventory = true;

}

void APickUp::BeginPlay()
{
	Super::BeginPlay();


	currentHealth = maxHealth;
	
}

void APickUp::PickedUp()
{
	if (bShallAppearInInventory)
	{
		AArcanumCloneCharacter* character = (AArcanumCloneCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
		if (character->StillSpaceInInventory(InventorySpace))
			character->PickUpItem(this);
		else
			UE_LOG(LogTemp, Warning, TEXT("Not enough Space in Inventory"));
	}
	Mesh->SetSimulatePhysics(false);
	SetActorEnableCollision(false);
	SetActorHiddenInGame(true);

	// Sound
	UGameplayStatics::SpawnSoundAttached(pickUpSound, GetRootComponent());
}

FString  APickUp::GetToolTip()
{
	return ToolTip;
}

UTexture2D* APickUp::GetTexture()
{
	return Icon;
}

void APickUp::Use()
{
	UE_LOG(LogTemp, Warning, TEXT("You shouldn't see this."));
}

void APickUp::Sell(AArcanumCloneCharacter* player, ABaseCharacter* merchant)
{
	if (bIsQuestRelevant)
		return;
	player->SetMoney(player->GetMoney() + price * 0.5f);
	merchant->AddToInventory(this);
}

void APickUp::Buy(AArcanumCloneCharacter * player)
{
	
	if (player->GetMoney() < price * 1.2f)
		return;
	else
	{
		player->SetMoney(player->GetMoney() - price * 1.2f);
		player->AddToInventory(this);
	}
}

bool APickUp::IsAttackable()
{
	return bAttackable;
}