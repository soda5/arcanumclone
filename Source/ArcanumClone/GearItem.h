// Mischa Ahi / Philip Gandy @ 2016

#pragma once

#include "PickUp.h"
#include "GearItem.generated.h"

UCLASS()
class ARCANUMCLONE_API AGearItem : public APickUp
{
	GENERATED_BODY()

public:

	AGearItem();

	UFUNCTION(BlueprintCallable, Category = "Equipment")
		virtual void GiveStats();

	UFUNCTION(BlueprintCallable, Category = "Equipment")
		virtual void RemoveStats();

};