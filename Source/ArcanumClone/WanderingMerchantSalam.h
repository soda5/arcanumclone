// Mischa Ahi / Philip Gandy @ 2016

#pragma once

#include "Merchant.h"
#include "MerchantsPendant.h"
#include "WanderingMerchantSalam.generated.h"

/**
 *
 */
UCLASS()
class ARCANUMCLONE_API AWanderingMerchantSalam : public AMerchant
{
	GENERATED_BODY()

public:

	AWanderingMerchantSalam();

	virtual void SetText() override;

	virtual int ReturnNextDialogPosition(int choosenAnswer) override;

	UFUNCTION(BlueprintImplementableEvent, Category = "Shop")
		void OpenShop();

	UFUNCTION(BlueprintCallable, Category = "Dialog")
		void GetReferences();

private:

	UWorld* world;

	ABaseCharacter* playerCharacter;

};
