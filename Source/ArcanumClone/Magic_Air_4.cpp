// Mischa Ahi / Philip Gandy @ 2016

#include "ArcanumClone.h"
#include "Magic_Air_4.h"
#include "BaseCharacter.h"

UMagic_Air_4::UMagic_Air_4()
{
	ManaCosts = 25;
	bIsInstant = false;
}

void UMagic_Air_4::Costs(UWorld * world)
{
	Super::Costs(world);

	if (world)
	{
		APlayerController* controller = world->GetFirstPlayerController();

		if (controller)
		{
			ACharacter* character = controller->GetCharacter();
			if (character)
			{
				ABaseCharacter* playerCharacter = dynamic_cast<ABaseCharacter*>(character);
				if (playerCharacter->GetCurrentMana() >= ManaCosts)
				{
					playerCharacter->SetCurrentMana(-ManaCosts);
					bManaCostsHasBeenPayed = true;
				}
				else
					bManaCostsHasBeenPayed = false;
			}
		}
	}
}

void UMagic_Air_4::ActivateSpell(ABaseCharacter * target, UWorld * world)
{
	Super::ActivateSpell(target, world);

	if (world)
	{
		APlayerController* controller = world->GetFirstPlayerController();

		if (controller)
		{
			ACharacter* character = controller->GetCharacter();
			if (character)
			{
				ABaseCharacter* playerCharacter = dynamic_cast<ABaseCharacter*>(character);

				if (bManaCostsHasBeenPayed)
				{
					if (target->Mesh_Air)
					{
						target->GetMesh()->SetSkeletalMesh(playerCharacter->Mesh_Air, false);
						target->BonusMovementSpeed += 10;
						target->AddArmor(5);
					}
				}
			}
		}
	}
}


