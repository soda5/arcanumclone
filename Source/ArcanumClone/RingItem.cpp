// Mischa Ahi / Philip Gandy @ 2016

#include "ArcanumClone.h"
#include "RingItem.h"

ARingItem::ARingItem()
{
	Name = "RingItem";

	Tags.Add("Ring");
}

void ARingItem::Use()
{
	AArcanumCloneCharacter* playerCharacter = (AArcanumCloneCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	playerCharacter->EquipItem(this);
}

void ARingItem::GiveStats()
{
	AArcanumCloneCharacter* playerCharacter = (AArcanumCloneCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	playerCharacter->AddArmor(armorvalue);
	playerCharacter->DamageModifier += damageModifier;
}

void ARingItem::RemoveStats()
{
	AArcanumCloneCharacter* playerCharacter = (AArcanumCloneCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	playerCharacter->AddArmor(-armorvalue);
	playerCharacter->DamageModifier += -damageModifier;
}