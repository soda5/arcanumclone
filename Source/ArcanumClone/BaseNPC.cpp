// Mischa Ahi @ 2016

#include "ArcanumClone.h"
#include "BaseNPC.h"

ABaseNPC::ABaseNPC()
{
	dialoguePosition = 0;
	SetText();

	bEndDialogHere = false;

	bStateMachineActive = false;
}

void ABaseNPC::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

FString ABaseNPC::GetDialogue()
{
	return dialogueSentence;
}

TArray<FString> ABaseNPC::GetAnswers()
{
	return dialogueOptions;
}

void ABaseNPC::StartDialogue()
{
	SetText();
}

int ABaseNPC::GetDialoguePosition()
{
	return dialoguePosition;
}

void ABaseNPC::SetDialoguePosition(int position)
{
	dialoguePosition = position;
}

void ABaseNPC::SetText()
{
	dialogueOptions.Empty();

	if (dialoguePosition == -1)
		dialoguePosition = 0;

	if (dialoguePosition == 0)
	{
		dialogueSentence = "Hi there.";
		dialogueOptions.Emplace("Hello Peasant.");
		dialogueOptions.Emplace("Hello fellow Citizen.");
		dialogueOptions.Emplace("And Bye there.");
	}

	if (dialoguePosition == 1)
	{
		dialogueSentence = "What do you want?";
		dialogueOptions.Emplace("I need something to release my Fury and Anger!");
		dialogueOptions.Emplace("I wanna pick something up.");
		dialogueOptions.Emplace("I want you to shut the f%ck up!");
	}

	if (dialoguePosition == 2)
	{
		dialogueSentence = "There should be a red, destroyable Box somewhere around here";
		dialogueOptions.Emplace("Thank you my Friend. Until next time.");
	}

	if (dialoguePosition == 3)
	{
		dialogueSentence = "I think there is a gray Box you can pick up inside this Level";
		dialogueOptions.Emplace("Lovely Loot should be lovely. See you soon my Friend.");
	}

	if (dialoguePosition == 4)
	{
		dialogueSentence = "Okay, then Bye.";
		dialogueOptions.Emplace("Until we meet Again.");
		dialogueOptions.Emplace("Smell ya.");
		dialogueOptions.Emplace("Get Lost!");
	}

	if (dialoguePosition == 5)
	{
		dialogueSentence = "Screw You!";
	}

	numberOfAnswers = dialogueOptions.Num();
}

int ABaseNPC::ReturnNextDialogPosition(int choosenAnswer)
{
	if (!choosenAnswer)
		return -1;

	if (choosenAnswer != 3)
	{
		bEndDialogHere = false;
		if (dialoguePosition == 0)
		{
			if (choosenAnswer == 1 || choosenAnswer == 2)
				dialoguePosition = 1;
		}
		else if (dialoguePosition == 1)
		{
			if (choosenAnswer == 1)
				dialoguePosition = 2;
			else if (choosenAnswer == 2)
				dialoguePosition = 3;
		}
		else if (dialoguePosition == 2 || dialoguePosition == 3)
			dialoguePosition = 4;
		else if (dialoguePosition == 4)
		{
			if (choosenAnswer == 1 || choosenAnswer == 2)
			{
				dialoguePosition = 0;
				bEndDialogHere = true;
			}
		}
	}
	else
		dialoguePosition = 5;

	return dialoguePosition;
}

int ABaseNPC::GetNumberOfAnswers()
{
	return numberOfAnswers;
}

bool ABaseNPC::IsThisTheEnd()
{
	return bEndDialogHere;
}