// Mischa Ahi / Philip Gandy @ 2016

#pragma once

#include "BrainlessEnemy.h"
#include "Retep.generated.h"

UCLASS()
class ARCANUMCLONE_API ARetep : public ABrainlessEnemy
{
	GENERATED_BODY()

public:

	ARetep();

	virtual void SpawnLoot() override;

	virtual void Death() override;

	UPROPERTY(EditAnywhere, Category = "Peniskopf")
		TSubclassOf<APickUp> EpicLoot;

};