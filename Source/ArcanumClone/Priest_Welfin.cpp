// Mischa Ahi / Philip Gandy @ 2016

#include "ArcanumClone.h"
#include "Priest_Welfin.h"

APriest_Welfin::APriest_Welfin()
{
	dialoguePosition = 1;

	
}

void APriest_Welfin::SetText()
{
	dialogueOptions.Empty();
	ButtonCounter = 0;

	switch (dialoguePosition)
	{
	case 1:
		dialogueSentence = TEXT("I don't seem to remember your face. You have to be new, I am Welfin, the priest of this little church. I am also the Mayor and the only Inhabitant of this forsaken Village. Let me tell you this was once a beautiful little Village with people resting here from there travels. But a little while ago all of this changed.");
		dialogueOptions.Emplace(TEXT("What happend?"));
		dialogueOptions.Emplace(TEXT("Old Man I do not have time for the story of your life. If you have something to tell me just cut this chit chat."));
		dialogueOptions.Emplace(TEXT("I don't care about history my friend. The Past is the Past, we cannot change what happend then."));
		ButtonCounter += 3;
		break;
	case 2:
		dialogueSentence = TEXT("The Goblins happend. They started to live in the Forest to the north of here. We only noticed them after they started to ambush the Traveler in the Forest, now it is called 'Goblin's Bush'. Since it is a rather small Forest most Travelers who ended arrived in this Village choose to took the Scenic Route and therefore entered the Forest.");
		dialogueOptions.Emplace(TEXT("The Goblin's Bush? really?"));
		dialogueOptions.Emplace(TEXT("But Goblins aren't strong fighters. How come no one hunted them down?"));
		dialogueOptions.Emplace(TEXT("I smell a reward for the one who get's rid of the Goblin pest."));
		dialogueOptions.Emplace(TEXT("For the right price I would offer you my assistance."));
		dialogueOptions.Emplace(TEXT("I am not interested in such lowly beeings as Goblins. To fight them would be a waste of my time."));
		dialogueOptions.Emplace(TEXT("If there are Goblins in the Forrest how am I supposed to get out of here?"));
		dialogueOptions.Emplace(TEXT("I am rather sorry, but I can't help you in these kinds of matters. I am a rather weak fighter."));
		ButtonCounter += 7;
		break;
	case 3:
		dialogueSentence = TEXT("I won't allow anyone to speak to me in this way. Mind your manners or I won't give you any Informations. Shall I continue?");
		dialogueOptions.Emplace(TEXT("That won't be needed I am leaving."));
		dialogueOptions.Emplace(TEXT("Sorry, I just want to get out of here. Please continue."));
		dialogueOptions.Emplace(TEXT("I didn't mean to hurt you. I am sincerly sorry. Please continue."));
		dialogueOptions.Emplace(TEXT("Do as you please."));
		ButtonCounter += 4;
		break;
	case 4:
		dialogueSentence = TEXT("It is quite a small forest. so from afar it really just looks like a bush.");
		dialogueOptions.Emplace(TEXT("I still have some questions."));
		dialogueOptions.Emplace(TEXT("That's all I wanted to know. See you soon."));
		ButtonCounter += 2;
		break;
	case 5:
		dialogueSentence = TEXT("Even thought Travelers rested in our small Village, we never asked for any money since we were happy enough when they told us about their Tales and Travels. Therefore we couldn't afford any Hunter or Mercenaries. The ones we could afford were quickly overwhelmed by the amount of Goblins.");
		dialogueOptions.Emplace(TEXT("You didn't took any money for your work?? that's quite foolish, isn't it?"));
		dialogueOptions.Emplace(TEXT("For the right amount of Money I would offer to lend you my strenght."));
		dialogueOptions.Emplace(TEXT("As long as you can reward me afterwards I might get rid of all these Goblins for you."));
		dialogueOptions.Emplace(TEXT("I don't have time for these matters right now."));
		dialogueOptions.Emplace(TEXT("I still have some questions."));
		ButtonCounter += 5;
		break;
	case 6:
		dialogueSentence = TEXT("No. I can't allow anyone to be killed. You must not fight them. There are to many.");
		if (playerCharacter->GetStrength() > 9)
		{
			dialogueOptions.Emplace(TEXT("Do I look that weak, that I can't even handle a few Goblins?"));
			ButtonCounter++;
		}
		if (playerCharacter->GetCharisma() > 12)
		{
			dialogueOptions.Emplace(TEXT("I am sure that if we work together we can easily defeat these Goblins."));
			ButtonCounter++;
		}
		if (playerCharacter->GetCharisma() > 9)
		{
			dialogueOptions.Emplace(TEXT("Let me tell you, I am quite a well known Mercenary. Until now none of my Enemies has survived an encounter with me. They call me 'The Wizzard'."));
			ButtonCounter++;
		}
		if (playerCharacter->GetDexterity() > 10)
		{
			dialogueOptions.Emplace(TEXT("There is no way that a mere Goblin could catch me! Even if I am in a bad Situation I can still run."));
			ButtonCounter++;
		}
		dialogueOptions.Emplace(TEXT("Than how can I get out of here?"));
		dialogueOptions.Emplace(TEXT("Even if you don't reward me I will kill these Goblins. Only for my personal pleasure."));
		dialogueOptions.Emplace(TEXT("Okay. Can I help you in any other way?"));
		dialogueOptions.Emplace(TEXT("I still have some questions."));
		ButtonCounter += 4;
		break;
	case 7:
		dialogueSentence = TEXT("I don't know any ways to leave this Village besides the Forest, but a few days ago a Merchant arrived here. maybe he knows something.");
		dialogueOptions.Emplace(TEXT("Thank you I will see what he can tell me."));
		dialogueOptions.Emplace(TEXT("Until we meet again."));
		dialogueOptions.Emplace(TEXT("Finally some useful Informations."));
		ButtonCounter += 3;
		break;
	case 8:
		dialogueSentence = TEXT("That is perfectly fine. I don't want you to. It is much to risky to fight so many Enemies at once.");
		dialogueOptions.Emplace(TEXT("I changed my mind. I don't like to be underestimated."));
		dialogueOptions.Emplace(TEXT("Good. Then we are on the same page. Is there a way to leave this Village?"));
		dialogueOptions.Emplace(TEXT("Then why did you tell me all of this?"));
		dialogueOptions.Emplace(TEXT("I still have some questions."));
		ButtonCounter += 4;
		break;
	case 9:
		dialogueSentence = TEXT("Since you don't want to talk to me what are you doing here? Get lost!");
		if (playerCharacter->GetBeauty() > 12)
		{
			dialogueOptions.Emplace(TEXT("I am really sorry. I s there any way you can forgive me? anything is fine."));
			ButtonCounter++;
		}
		if (playerCharacter->GetCharisma() > 9)
		{
			dialogueOptions.Emplace(TEXT("I sincerly apologize. I showed you my rude and selfish side for no reason."));
			ButtonCounter++;
		}
		dialogueOptions.Emplace(TEXT("I am sorry."));
		dialogueOptions.Emplace(TEXT("Fine."));
		dialogueOptions.Emplace(TEXT("..."));
		ButtonCounter += 3;
		break;
	case 10:
		dialogueSentence = TEXT("Sure, what do you want to know?");
		dialogueOptions.Emplace(TEXT("The Goblin's Bush? really?"));
		dialogueOptions.Emplace(TEXT("But Goblins aren't strong fighters. How come no one hunted them down?"));
		dialogueOptions.Emplace(TEXT("I smell a reward for the one who get's rid of the Goblin pest."));
		dialogueOptions.Emplace(TEXT("For the right price I would offer you my assistance."));
		dialogueOptions.Emplace(TEXT("Is there any way to leave?"));
		dialogueOptions.Emplace(TEXT("That's all. Have a nice day."));
		ButtonCounter += 6;
		break;
	case 11:
		dialogueSentence = TEXT("Hindsight is 20/20. At the time we were a small Village far away from the next Town. We didn't use any money.");
		dialogueOptions.Emplace(TEXT("..."));
		dialogueOptions.Emplace(TEXT("I still have some questions."));
		dialogueOptions.Emplace(TEXT("That's all I need to know."));
		ButtonCounter += 3;
		break;
	case 12:
		dialogueSentence = TEXT("You might have a point there. Maybe you can really get rid of these Goblins.");
		dialogueOptions.Emplace(TEXT("What about the reward?"));
		dialogueOptions.Emplace(TEXT("Consider it done."));
		dialogueOptions.Emplace(TEXT("Nevermind I don't want to risk my life."));
		dialogueOptions.Emplace(TEXT("I still have some questions."));
		ButtonCounter += 4;
		break;
	case 13:
		dialogueSentence = TEXT("Oh God please don't. We don't want no mindless bloodshed. If you go through with this I will stop talking with you.");
		dialogueOptions.Emplace(TEXT("I do not care about your opinion, priest."));
		dialogueOptions.Emplace(TEXT("Just kidding."));
		ButtonCounter += 2;
		break;
	case 14:
		dialogueSentence = TEXT("There are two things bothering me right now. The first problem is that a small group of Goblins started to live in the Cave to the east of the Village.");
		dialogueOptions.Emplace(TEXT("So you want me to get rid of these Goblins but not the others?"));
		dialogueOptions.Emplace(TEXT("What is the other problem?"));
		dialogueOptions.Emplace(TEXT("Sorry but I don't want to kill."));
		dialogueOptions.Emplace(TEXT("Sure Consider it Done."));
		ButtonCounter += 4;
		break;
	case 15:
		dialogueSentence = TEXT("Please don't. I really don't want you to die.");
		dialogueOptions.Emplace(TEXT("Fine."));
		dialogueOptions.Emplace(TEXT("I was just kidding."));
		dialogueOptions.Emplace(TEXT("No, I've heard enough."));
		ButtonCounter += 3;
		break;
	case 16:
		dialogueSentence = TEXT("Well, you see, I am kinda lonely out here all alone. So I got exhited to speak with someone once again.");
		dialogueOptions.Emplace(TEXT("I see ..."));
		dialogueOptions.Emplace(TEXT("Nevermind then, I still got some questions."));
		dialogueOptions.Emplace(TEXT("That's all I needed to know."));
		ButtonCounter += 3;
		break;
	case 17:
		dialogueSentence = TEXT("Fine but just this once I will forgive you. So what do you want.");
		dialogueOptions.Emplace(TEXT("I still have some questions."));
		ButtonCounter += 1;
		break;
	case 18:
		dialogueSentence = TEXT("...  Is there something you want to know?");
		dialogueOptions.Emplace(TEXT("Yes."));
		dialogueOptions.Emplace(TEXT("No."));
		ButtonCounter += 2;
		break;
	case 19:
		dialogueSentence = TEXT("I can't give you much. But I can give you either this ring I found a long time ago inside the church or about 150 gold I safed.");
		dialogueOptions.Emplace(TEXT("I want the ring."));
		dialogueOptions.Emplace(TEXT("I take the money."));
		dialogueOptions.Emplace(TEXT("Can't you give me both. I mean I am risking my life."));
		ButtonCounter += 3;
		break;
	case 20:
		dialogueSentence = TEXT("You again. What do you want from me?");
		dialogueOptions.Emplace(TEXT("I still have some questions."));
		dialogueOptions.Emplace(TEXT("nevermind."));
		if (playerCharacter->bFinishedAttackCaveGoblinsObjective)
		{
			dialogueOptions.Emplace(TEXT("I killed all the Goblins in the Cave."));
			ButtonCounter++;
		}
		if (playerCharacter->bFoundMerchantsPendant)
		{
			dialogueOptions.Emplace(TEXT("I got the Merchants Pendant."));
			ButtonCounter++;
		}
		ButtonCounter += 2;
		break;
	case 21:
		dialogueSentence = TEXT("That wasn't even funny.");
		dialogueOptions.Emplace(TEXT("I still have some questions."));
		ButtonCounter += 1;
		break;
	case 22:
		dialogueSentence = TEXT("Exactly.");
		dialogueOptions.Emplace(TEXT("Fine. And the other problem?"));
		dialogueOptions.Emplace(TEXT("Not going to happen. I don't kill for such reasons."));
		dialogueOptions.Emplace(TEXT("Not going to happen. I don't kill."));
		ButtonCounter += 3;
		break;
	case 23:
		dialogueSentence = TEXT("The Merchant ownes a really nice pendant that intrigues me. If you could get it for me, I might find some useful stuff in the church.");
		dialogueOptions.Emplace(TEXT("I see what I can do."));
		ButtonCounter += 1;
		break;
	case 24:
		dialogueSentence = TEXT("No way. Are you crazy. Decide!");
		dialogueOptions.Emplace(TEXT("Then the Ring will do."));
		dialogueOptions.Emplace(TEXT("This world is ruled by money so of course I choose the money."));
		ButtonCounter += 2;
		break;
	case 25:
		dialogueSentence = TEXT("Fine. You are right, I will give you both once the quest is completed.");
		dialogueOptions.Emplace(TEXT("Thanks."));
		ButtonCounter += 1;
		break;
	case 26:
		dialogueSentence = TEXT("You really killed all the Goblins in the Cave. Unbelievable.");
		dialogueOptions.Emplace(TEXT("That was a piece of Cake."));
		dialogueOptions.Emplace(TEXT("So about the Goblins in the Forrest."));
		ButtonCounter += 2;
		break;
	case 27:
		dialogueSentence = TEXT("That is the Pendant isn't it?");
		dialogueOptions.Emplace(TEXT("Indeed it is."));
		ButtonCounter += 1;
		break;
	case 28:
		dialogueSentence = TEXT("I think you might be able to kill all the Goblins in the Forrest as well.");
		dialogueOptions.Emplace(TEXT("Consider it done."));
		dialogueOptions.Emplace(TEXT("Let's talk about the reward."));
		ButtonCounter += 2;
		break;
	case 29:
		dialogueSentence = TEXT("I can trade you three Healthpotions for it.");
		dialogueOptions.Emplace(TEXT("That seems fair."));
		dialogueOptions.Emplace(TEXT("No I don't want to trade this. You were right it is intriguing."));
		if (playerCharacter->GetCharisma() > 14 || playerCharacter->GetIntelligence() > 14)
		{
			dialogueOptions.Emplace(TEXT("Make it 5 and we have a deal."));
			ButtonCounter++;
		}
		ButtonCounter += 2;
		break;
	default:
		dialogueSentence = TEXT("ERROR!ERROR!ERROR!");
		dialogueOptions.Emplace(TEXT("RESET DIALOGTREE..."));
		ButtonCounter += 1;
		break;
	}
	numberOfAnswers = dialogueOptions.Num();
}

int APriest_Welfin::ReturnNextDialogPosition(int choosenAnswer)
{

	if (!choosenAnswer)
		return 1;
	switch (dialoguePosition)
	{
	case 1:
		if (choosenAnswer == 1)
			dialoguePosition = 2;
		else
			dialoguePosition = 3;
		break;
	case 2:
		if (choosenAnswer == 1)
			dialoguePosition = 4;
		else if (choosenAnswer == 2)
			dialoguePosition = 5;
		else if (choosenAnswer == 3 || choosenAnswer == 4)
			dialoguePosition = 6;
		else if (choosenAnswer == 6)
			dialoguePosition = 7;
		else
			dialoguePosition = 8;
		break;
	case 3:
		if (choosenAnswer == 1 || choosenAnswer == 4)
		{
			bEndDialogHere = true;
			dialoguePosition = 9;
		}
		else
			dialoguePosition = 2;
		break;
	case 4:
		if (choosenAnswer == 1)
			dialoguePosition = 10;
		else
		{
			bEndDialogHere = true;
			dialoguePosition = 20;
		}
		break;
	case 5:
		if (choosenAnswer == 1)
			dialoguePosition = 11;
		else if (choosenAnswer == 4)
		{
			bEndDialogHere = true;
			dialoguePosition = 20;
		}
		else if (dialoguePosition == 5)
			dialoguePosition = 10;
		else
			dialoguePosition = 6;
		break;
	case 6:
		extraAnswers = ButtonCounter - 4;
		if (extraAnswers > 0)
			choosenAnswer -= extraAnswers;
		if (choosenAnswer == 1)
			dialoguePosition = 7;
		else if (choosenAnswer == 2)
			dialoguePosition = 13;
		else if (choosenAnswer == 3)
			dialoguePosition = 14;
		else if (choosenAnswer == 4)
			dialoguePosition = 10;
		else
			dialoguePosition = 12;
		break;
	case 7:
		bEndDialogHere = true;
		dialoguePosition = 20;
		break;
	case 8:
		if (choosenAnswer == 1)
			dialoguePosition = 15;
		else if (choosenAnswer == 2)
			dialoguePosition = 7;
		else if (choosenAnswer == 3)
			dialoguePosition = 16;
		else
			dialoguePosition = 10;
		break;
	case 9:
		extraAnswers = ButtonCounter - 3;
		if (extraAnswers > 0)
			choosenAnswer -= extraAnswers;
		if (choosenAnswer <= 0)
		{
			bEndDialogHere = false;
			dialoguePosition = 17;
		}
		else
			dialoguePosition = 9;
		break;
	case 10:
		if (choosenAnswer == 1)
			dialoguePosition = 4;
		else if (choosenAnswer == 2)
			dialoguePosition = 5;
		else if (choosenAnswer == 5)
			dialoguePosition = 7;
		else if (choosenAnswer == 6)
		{
			bEndDialogHere = true;
			dialoguePosition = 20;
		}
		else
			dialoguePosition = 6;
		break;
	case 11:
		if (choosenAnswer == 1)
			dialoguePosition = 18;
		else if (choosenAnswer == 2)
			dialoguePosition = 10;
		else
		{
			bEndDialogHere = true;
			dialoguePosition = 20;
		}
		break;
	case 12:
		if (choosenAnswer == 1)
			dialoguePosition = 19;
		else if (choosenAnswer == 2)
		{
			bEndDialogHere = true;
			dialoguePosition = 20;
		}
		else if (choosenAnswer == 3)
			dialoguePosition = 8;
		else
			dialoguePosition = 10;
		break;
	case 13:
		if (choosenAnswer == 1)
		{
			bEndDialogHere = true;
			dialoguePosition = 9;
		}
		else
			dialoguePosition = 21;
		break;
	case 14:
		playerCharacter->bStartedAttackCaveGoblins = true;
		if (choosenAnswer == 1)
			dialoguePosition = 22;
		else if (choosenAnswer == 4)
		{
			bEndDialogHere = true;
			dialoguePosition = 20;
		}
		else
			dialoguePosition = 23;
		break;
	case 15:
		if (choosenAnswer == 1)
			dialoguePosition = 18;
		else if (choosenAnswer == 2)
			dialoguePosition = 21;
		else
		{
			bEndDialogHere = true;
			dialoguePosition = 9;
		}
		break;
	case 16:
		if (choosenAnswer == 1)
			dialoguePosition = 18;
		else if (choosenAnswer == 2)
			dialoguePosition = 10;
		else
		{
			bEndDialogHere = true;
			dialoguePosition = 20;
		}
		break;
	case 17:
		dialoguePosition = 10;
		break;
	case 18:
		if (choosenAnswer == 1)
			dialoguePosition = 10;
		else
		{
			bEndDialogHere = true;
			dialoguePosition = 20;
		}
		break;
	case 19:
		if (choosenAnswer == 3)
		{
			if (playerCharacter->GetCharisma() > 16 || playerCharacter->GetIntelligence() > 14)
			{
				dialoguePosition = 25;
				bMoneyWasChoosen = true;
				bRingWasChoosen = true;
			}
			else
				dialoguePosition = 24;
		}
		else
		{
			if (choosenAnswer == 1)
			{
				bMoneyWasChoosen = false;
				bRingWasChoosen = true;
			}
			else if (choosenAnswer == 2)
			{
				bMoneyWasChoosen = true;
				bRingWasChoosen = false;
			}
			bEndDialogHere = true;
			dialoguePosition = 20;
		}
		break;
	case 20:
		if (choosenAnswer != 2)
			bEndDialogHere = false;
		if (choosenAnswer == 1)
			dialoguePosition = 10;
		else if (choosenAnswer == 2)
			dialoguePosition = 20;
		else if (playerCharacter->bFinishedAttackCaveGoblinsObjective)
		{
			if (playerCharacter->bFoundMerchantsPendant)
			{
				if (choosenAnswer == 3)
					dialoguePosition = 26;
				else
					dialoguePosition = 27;
				break;
			}
			dialoguePosition = 26;
		}
		else if (playerCharacter->bFoundMerchantsPendant)
			dialoguePosition = 27;
		break;
	case 21:
		dialoguePosition = 10;
		break;
	case 22:
		dialoguePosition = 23;
		break;
	case 23:
		playerCharacter->bStartedMerchantsPendant = true;
		bEndDialogHere = true;
		dialoguePosition = 20;
		break;
	case 24:
		bEndDialogHere = true;
		dialoguePosition = 20;
		break;
	case 25:
		bEndDialogHere = true;
		dialoguePosition = 20;
		break;
	case 26:
		dialoguePosition = 28;
		break;
	case 27:
		dialoguePosition = 29;
		break;
	case 28:
		if (choosenAnswer == 1)
		{
			bEndDialogHere = true;
			dialoguePosition = 20;
		}
		else
			dialoguePosition = 19;
		break;
	case 29:
		bEndDialogHere = true;
		if (choosenAnswer == 2)
		{
			dialoguePosition = 9;
			break;
		}
		else
			dialoguePosition = 20;

		if (choosenAnswer == 3)
			amountOfHeealthpotions = 5;
		for (int i = 0; i < amountOfHeealthpotions; i++)
		{
			world->SpawnActor<AHealthPotion>(Healthpotion, GetActorLocation() + FVector(0, -i * 50, 0), FRotator(0, 0, 0));
		}
			playerCharacter->RemoveFromInventory(Pendant);
			playerCharacter->bFoundMerchantsPendant = false;
		break;
	default:

		break;
	}

	return dialoguePosition;
}

void APriest_Welfin::GetReferences()
{
	playerCharacter = (AArcanumCloneCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	world = GetWorld();

	playerCharacter->bTalkedWithThePriest = true;
	
	Pendant = world->SpawnActor<AMerchantsPendant>(QuestPendantReference, GetActorLocation(), FRotator(0, 0, 0));
	Pendant->Mesh->SetSimulatePhysics(false);
	Pendant->SetActorEnableCollision(false);
	Pendant->SetActorHiddenInGame(true);
}
