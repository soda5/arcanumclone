// Mischa Ahi / Philip Gandy @ 2016

#include "ArcanumClone.h"
#include "Magic_Air_2.h"
#include "BaseCharacter.h"

UMagic_Air_2::UMagic_Air_2()
{
	ManaCosts = 10;
	PushRange = 500;
}

void UMagic_Air_2::Costs(UWorld* world)
{
	Super::Costs(world);

	if (world)
	{
		APlayerController* controller = world->GetFirstPlayerController();

		if (controller)
		{
			ACharacter* character = controller->GetCharacter();
			if (character)
			{
				ABaseCharacter* playerCharacter = dynamic_cast<ABaseCharacter*>(character);
				if (playerCharacter->GetCurrentMana() >= ManaCosts)
				{
					playerCharacter->SetCurrentMana(-ManaCosts);
					bManaCostsHasBeenPayed = true;
				}
				else
					bManaCostsHasBeenPayed = false;
			}
		}
	}
}

void UMagic_Air_2::ActivateSpell(ABaseCharacter * target, UWorld * world)
{
	Super::ActivateSpell(target, world);

	if (world)
	{
		APlayerController* controller = world->GetFirstPlayerController();

		if (controller)
		{
			ACharacter* character = controller->GetCharacter();
			if (character)
			{
				ABaseCharacter* playerCharacter = dynamic_cast<ABaseCharacter*>(character);
				
				if (bManaCostsHasBeenPayed)
				{
					FVector playerPosition = playerCharacter->GetActorLocation();
					FVector targetPosition = target->GetActorLocation();

					FVector recoil = playerPosition - targetPosition;

					float length = recoil.Dist(playerPosition, targetPosition);

					float x;

					x = PushRange / length;

					target->AddActorLocalOffset(recoil* x);
				}
			}
		}
	}
}
