// Mischa Ahi / Philip Gandy @ 2016

#pragma once

#include "BaseCharacter.h"
#include "PlayerSaveValues.generated.h"

USTRUCT(BlueprintType)
struct FPlayerValues
{
	GENERATED_BODY()

public:

	void SavePlayerValues(ABaseCharacter* character);

	void LoadPlayerValues(ABaseCharacter* character);

	UPROPERTY()
		FTransform transform;

	UPROPERTY()
		int NPCNumber;

	UPROPERTY(EditAnywhere)
		TArray<AGearItem*> Gear;

	UPROPERTY()
		TArray<FString> GearStrings;

	UPROPERTY()
		TArray<bool> LearnedSpells;

	UPROPERTY()
		int BonusMovementSpeed;

	UPROPERTY()
		bool bAlive;

	UPROPERTY()
		float WeaponAttackspeed;

	UPROPERTY()
		float DamageModifier;

	UPROPERTY()
		int rewardExp;

	UPROPERTY(EditAnywhere)
		UTexture2D* Icon;

	UPROPERTY(EditAnywhere)
		FString name;

	UPROPERTY(EditAnywhere)
		FString race;

	UPROPERTY(EditAnywhere)
		FString sex;

	UPROPERTY()
		int level;

	UPROPERTY()
		int skillPoints;

	UPROPERTY()
		int xp;

	UPROPERTY()
		int healthGrowth;

	UPROPERTY()
		int manaGrowth;

	UPROPERTY()
		int money;

	UPROPERTY()
		int ammunition;

	UPROPERTY()
		int strength;

	UPROPERTY()
		int intelligence;

	UPROPERTY()
		int stamina;

	UPROPERTY()
		int willPower;

	UPROPERTY()
		int dexterity;

	UPROPERTY()
		int perception;

	UPROPERTY()
		int beauty;

	UPROPERTY()
		int charisma;

	UPROPERTY()
		int maxHealth;

	UPROPERTY()
		int currentHealth;

	UPROPERTY()
		int maxMana;

	UPROPERTY()
		int currentMana;

	UPROPERTY()
		int armor;

	// Skills
	UPROPERTY()
		int bow;

	UPROPERTY()
		int dodge;

	UPROPERTY()
		int meele;

	UPROPERTY()
		int throwing;

	UPROPERTY()
		int backStab;

	UPROPERTY()
		int pickPocket;

	UPROPERTY()
		int prowling;

	UPROPERTY()
		int spotTrap;

	UPROPERTY()
		int gambling;

	UPROPERTY()
		int haggle; // feilschen

	UPROPERTY()
		int heal;

	UPROPERTY()
		int persuation; // überzeugen

	UPROPERTY()
		int repair;

	UPROPERTY()
		int firearms;

	UPROPERTY()
		int pickLocks; // schlösserknacken

	UPROPERTY()
		int disarmTraps;

	// General values

	UPROPERTY()
		int carryWeight;

	UPROPERTY()
		int movementSpeed;

	UPROPERTY()
		int spellLimit;

	UPROPERTY()
		int karma;

	UPROPERTY(EditAnywhere)
		TArray<APickUp*> inventory;

	UPROPERTY()
		TArray<FString> inventoryStrings; //for saving through levelchange

	UPROPERTY()
		int inventorySpace;

	UPROPERTY()
		int alignment; // ausrichtung technologie oder magie

	UPROPERTY()
		float timeSinceLastAttack;

	UPROPERTY()
		float attackCooldown;

	UPROPERTY()
		bool bHelmetVisible;

	UPROPERTY()
		bool bStartedAttackCaveGoblins;

	UPROPERTY()
		bool bStartedMerchantsPendant;

	UPROPERTY()
		bool bFinishedAttackCaveGoblinsObjective;

	UPROPERTY()
		bool bFoundMerchantsPendant;

	UPROPERTY()
		bool bTalkedWithThePriest;
};