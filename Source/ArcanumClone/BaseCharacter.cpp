// Mischa Ahi @ 2016

#include "ArcanumClone.h"
#include "PickUp.h"
#include "BaseCharacter.h"
#include "GearItem.h"
#include "Kismet/KismetMathLibrary.h"


ABaseCharacter::ABaseCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	//Spell 4 Meshes
	Mesh_Air = CreateDefaultSubobject<USkeletalMesh>(TEXT("AirMagicMesh"));
	Mesh_Earth = CreateDefaultSubobject<USkeletalMesh>(TEXT("EarthMagicMesh"));
	Mesh_Water = CreateDefaultSubobject<USkeletalMesh>(TEXT("WaterMagicMesh"));
	Mesh_Fire = CreateDefaultSubobject<USkeletalMesh>(TEXT("FireMagicMesh"));

	//Equipment Meshes
	Helmet = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Helmet"));
	Helmet->SetupAttachment(GetMesh(), "head");
	Helmet->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Weapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Weapon"));
	Weapon->SetupAttachment(GetMesh(), "RightHandIndex1");
	Weapon->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	SpellBook = CreateDefaultSubobject<USpellBook>(TEXT("SpellBook"));

	ConstructorHelpers::FObjectFinder<UTexture2D> Texture(TEXT("Texture2D'/Game/Textures/Unknown_Protagonist.Unknown_Protagonist'"));

	icon = Texture.Object;

	level = 1;
	skillPoints = 5;
	xp = 0;
	strength = 8;
	intelligence = 8;
	stamina = 8;
	willPower = 8;
	dexterity = 8;
	perception = 8;
	beauty = 8;
	charisma = 8;

	// Init Health and Mana
	maxHealth = GetMaxHealth();
	currentHealth = maxHealth;
	maxMana = GetMaxMana();
	currentMana = maxMana;

	bow = 0;
	dodge = 0;
	meele = 0;
	pickPocket = 0;
	prowling = 0;
	gambling = 0;
	haggle = 0;
	persuation = 0;
	pickLocks = 0;

	inventorySpace = 50;

	carryWeight = GetCarryWeight();
	movementSpeed = GetMaxMovementSpeed();
	GetCharacterMovement()->MaxWalkSpeed = movementSpeed * 30;
	spellLimit = intelligence / 4;
	karma = 0;
	alignment = 0;
	AttackRange = 150;
	AttackSpeedDebuff = 0;

	State = 0;

	Gear.Init(NULL, 8);

	WeaponAttackspeed = 1;

	bStateMachineActive = true;

	bAlive = true;
	bPlayer = false;
	bHasAttackStarted = false;//so the character doesn't spawn with a fist and a swing
}

void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();
}

void ABaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	deltaTime = DeltaTime;

	timeSinceLastAttack += deltaTime;

	//
	if (MovementSlowDuration > 0)
		MovementSlowDuration -= deltaTime;
	else
		MovementSpeedSlowValue = 0;

	movementSpeed = GetMaxMovementSpeed();
	GetCharacterMovement()->MaxWalkSpeed = movementSpeed * 30;
	//

	//
	if (AttackSpeedDebuffDuration > 0)
		AttackSpeedDebuffDuration -= deltaTime;
	else
		AttackSpeedDebuff = 0;
	//

	if (timeSinceLastAttack >= GetAttackCooldown())
		bHasAttackStarted = false;

	// Change between States
	if (bStateMachineActive)
	{
		if (State == 0)
			Idle();
		else if (State == 1)
			Seek();
		else if (State == 2)
			Attack();
		else if (State == 3)
			Death();
		else
			UE_LOG(LogTemp, Warning, TEXT("Wenn du das hier liest, funktioniert die StateMachine nicht richtig!"));
	}
}

FString ABaseCharacter::GetName()
{
	return name;
}

void ABaseCharacter::SetName(FString name)
{
	this->name = name;
}

FString ABaseCharacter::GetRace()
{
	return race;
}

void ABaseCharacter::SetRace(FString race)
{
	this->race = race;
}

FString ABaseCharacter::GetSex()
{
	return sex;
}

int ABaseCharacter::GetLevel()
{
	return level;
}

int ABaseCharacter::GetSkillPoints()
{
	return skillPoints;
}

void ABaseCharacter::SetSkillPoints(int points)
{
	this->skillPoints = points;
}

void ABaseCharacter::IncreaseSkillPoints()
{
	skillPoints++;
}

int ABaseCharacter::GetXp()
{
	return xp;
}

void ABaseCharacter::SetXp(int xp)
{
	this->xp = xp;
}

int ABaseCharacter::GetMaxHealth()
{
	maxHealth = 6 + strength * 2 + willPower + healthGrowth * 4 + level * 2;

	return maxHealth;
}

void ABaseCharacter::SetMaxHealth(int health)
{
	this->maxHealth = health;
}

int ABaseCharacter::GetCurrentHealth()
{
	return currentHealth;
}

void ABaseCharacter::SetCurrentHealth(int healthChange)
{
	this->currentHealth = healthChange;
}

int ABaseCharacter::GetHealthGrowth()
{
	return healthGrowth;
}

void ABaseCharacter::SetHealthGrowth(int growth)
{
	this->healthGrowth = growth;
}

void ABaseCharacter::AddToCurrentHealth(int healthChange)
{
	currentHealth += healthChange;

	bStateMachineActive = true;
}

void ABaseCharacter::CheatHealth()
{
	currentHealth += 100;
}

int ABaseCharacter::GetMaxMana()
{
	maxMana = 6 + stamina * 2 + willPower + manaGrowth * 4 + level * 2;

	return maxMana;
}

void ABaseCharacter::SetMaxMana(int mana)
{
	this->maxMana = mana;
}

int ABaseCharacter::GetCurrentMana()
{
	return currentMana;
}

int ABaseCharacter::GetManaGrowth()
{
	return manaGrowth;
}

void ABaseCharacter::SetManaGrowth(int growth)
{
	this->manaGrowth = growth;
}

void ABaseCharacter::SetCurrentMana(int change)
{
	currentMana += change;
}

void ABaseCharacter::CheatMana()
{
	currentMana += 100;
}

int ABaseCharacter::GetCarryWeight()
{
	carryWeight = strength * 500;

	return carryWeight;
}

void ABaseCharacter::SetCarryWeight(int value)
{
	this->carryWeight = value;
}

int ABaseCharacter::GetMaxMovementSpeed()
{
	int speed = dexterity + BonusMovementSpeed - MovementSpeedSlowValue;

	if (speed > 0 && speed < 26)
		return speed;
	else
		return 1;
}

int ABaseCharacter::GetMovementSpeed()
{
	return movementSpeed;
}

void ABaseCharacter::SetMovementSpeed(int speed)
{
	this->movementSpeed = speed;
}

void ABaseCharacter::IncreaseStrength()
{
	if (skillPoints > 0)
	{
		skillPoints--;
		UndoSkillPoints++;
		AllChoosenSkills.Add(ELastGivenSkill::Strenght);
		strength++;

		maxHealth = GetMaxHealth();
		currentHealth += 2;
		carryWeight = GetCarryWeight();
	}
	else
		return;
}

void ABaseCharacter::IncreaseManaGrowth(int amount)
{
	currentMana += amount * 4;
	manaGrowth += amount;
}

void ABaseCharacter::IncreaseHealthGrowth(int amount)
{
	currentHealth += amount * 4;
	healthGrowth += amount;
}

void ABaseCharacter::IncreaseStrengthByMagic()
{
	strength++;

	maxHealth = GetMaxHealth();
	currentHealth += 2;
	carryWeight = GetCarryWeight();
}

void ABaseCharacter::DecreaseStrength()
{
	if (UndoSkillPoints != 0 && AllChoosenSkills.Last() == ELastGivenSkill::Strenght)
	{
		UndoSkillPoints--;
		AllChoosenSkills.Pop();
		strength--;
		skillPoints++;
	}
	else
		return;
}

int ABaseCharacter::GetStrength()
{
	return strength;
}

void ABaseCharacter::SetStrength(int strength)
{
	this->strength = strength;
}

void ABaseCharacter::IncreaseStamina()
{
	if (skillPoints > 0)
	{
		skillPoints--;
		UndoSkillPoints++;
		AllChoosenSkills.Add(ELastGivenSkill::Stamina);
		stamina++;

		maxMana = GetMaxMana();
		currentMana += 2;
	}
	else
		return;
}

void ABaseCharacter::IncreaseStaminaByMagic()
{
	stamina++;

	maxMana = GetMaxMana();
	currentMana += 2;
}

void ABaseCharacter::DecreaseStamina()
{
	if (UndoSkillPoints != 0 && AllChoosenSkills.Last() == ELastGivenSkill::Stamina)
	{
		UndoSkillPoints--;
		AllChoosenSkills.Pop();
		stamina--;
		skillPoints++;
	}
	else
		return;
}

int ABaseCharacter::GetStamina()
{
	return stamina;
}

void ABaseCharacter::SetStamina(int stamina)
{
	this->stamina = stamina;
}

void ABaseCharacter::IncreaseIntelligence()
{
	if (skillPoints > 0)
	{
		skillPoints--;
		UndoSkillPoints++;
		AllChoosenSkills.Add(ELastGivenSkill::Intelligence);
		intelligence++;

		spellLimit = CalculateSpellLimit();
	}
	else
		return;
}

void ABaseCharacter::IncreaseIntelligenceByMagic()
{
	intelligence++;

	spellLimit = CalculateSpellLimit();
}

void ABaseCharacter::DecreaseIntelligence()
{
	if (UndoSkillPoints != 0 && AllChoosenSkills.Last() == ELastGivenSkill::Intelligence)
	{
		UndoSkillPoints--;
		AllChoosenSkills.Pop();
		intelligence--;
		skillPoints++;
	}
	else
		return;
}

void ABaseCharacter::SetIntelligence(int intelligence)
{
	this->intelligence = intelligence;
}

int ABaseCharacter::GetIntelligence()
{
	return intelligence;
}

void ABaseCharacter::IncreaseWillPower()
{
	if (skillPoints > 0)
	{
		skillPoints--;
		UndoSkillPoints++;
		AllChoosenSkills.Add(ELastGivenSkill::Willpower);
		willPower++;

		maxHealth = GetMaxHealth();
		currentHealth++;
		maxMana = GetMaxMana();
		currentMana++;
	}
	else
		return;
}

void ABaseCharacter::DecreaseWillPower()
{
	if (UndoSkillPoints != 0 && AllChoosenSkills.Last() == ELastGivenSkill::Willpower)
	{
		UndoSkillPoints--;
		AllChoosenSkills.Pop();
		willPower--;
		skillPoints++;

		maxHealth = GetMaxHealth();
		currentHealth--;
		maxMana = GetMaxMana();
		currentMana--;
	}
	else
		return;
}

int ABaseCharacter::GetWillPower()
{
	return willPower;
}

void ABaseCharacter::SetWillPower(int will)
{
	this->willPower = will;
}

void ABaseCharacter::IncreaseDexterity()
{
	if (skillPoints > 0)
	{
		skillPoints--;
		UndoSkillPoints++;
		AllChoosenSkills.Add(ELastGivenSkill::Dexterity);
		dexterity++;
	}
	else
		return;
}

void ABaseCharacter::DecreaseDexterity()
{
	if (UndoSkillPoints != 0 && AllChoosenSkills.Last() == ELastGivenSkill::Dexterity)
	{
		UndoSkillPoints--;
		AllChoosenSkills.Pop();
		dexterity--;
		skillPoints++;
	}
	else
		return;
}

int ABaseCharacter::GetDexterity()
{
	return dexterity;
}

void ABaseCharacter::SetDexterity(int dex)
{
	this->dexterity = dex;
}

void ABaseCharacter::IncreasePerception()
{
	if (skillPoints > 0)
	{
		skillPoints--;
		UndoSkillPoints++;
		AllChoosenSkills.Add(ELastGivenSkill::Perception);
		perception++;
	}
	else
		return;
}

void ABaseCharacter::DecreasePerception()
{
	if (UndoSkillPoints != 0 && AllChoosenSkills.Last() == ELastGivenSkill::Perception)
	{
		UndoSkillPoints--;
		AllChoosenSkills.Pop();
		perception--;
		skillPoints++;
	}
	else
		return;
}

int ABaseCharacter::GetPerception()
{
	return perception;
}

void ABaseCharacter::SetPerception(int perception)
{
	this->perception = perception;
}

void ABaseCharacter::IncreaseBeauty()
{
	if (skillPoints > 0)
	{
		skillPoints--;
		UndoSkillPoints++;
		AllChoosenSkills.Add(ELastGivenSkill::Beauty);
		beauty++;
	}
	else
		return;
}

void ABaseCharacter::IncreaseBeautyByMagic()
{
	beauty++;
}

void ABaseCharacter::DecreaseBeauty()
{
	if (UndoSkillPoints != 0 && AllChoosenSkills.Last() == ELastGivenSkill::Beauty)
	{
		UndoSkillPoints--;
		AllChoosenSkills.Pop();
		beauty--;
		skillPoints++;
	}
	else
		return;
}

int ABaseCharacter::GetBeauty()
{
	return beauty;
}

void ABaseCharacter::SetBeauty(int beauty)
{
	this->beauty = beauty;
}

void ABaseCharacter::IncreaseCharisma()
{
	if (skillPoints > 0)
	{
		skillPoints--;
		UndoSkillPoints++;
		AllChoosenSkills.Add(ELastGivenSkill::Charisma);
		charisma++;
	}
	else
		return;
}

void ABaseCharacter::DecreaseCharisma()
{
	if (UndoSkillPoints != 0 && AllChoosenSkills.Last() == ELastGivenSkill::Charisma)
	{
		UndoSkillPoints--;
		AllChoosenSkills.Pop();
		charisma--;
		skillPoints++;
	}
	else
		return;
}

int ABaseCharacter::GetCharisma()
{
	return charisma;
}

void ABaseCharacter::SetCharisma(int charisma)
{
	this->charisma = charisma;
}

void ABaseCharacter::IncreaseHealth()
{
	if (skillPoints > 0)
	{
		skillPoints--;
		UndoSkillPoints++;
		AllChoosenSkills.Add(ELastGivenSkill::Health);
		healthGrowth++;
		currentHealth += 4;
	}
	else
		return;
}

void ABaseCharacter::DecreaseHealth()
{
	if (UndoSkillPoints != 0 && AllChoosenSkills.Last() == ELastGivenSkill::Health)
	{
		UndoSkillPoints--;
		AllChoosenSkills.Pop();
		currentHealth -= 4;
		healthGrowth--;
		skillPoints++;
	}
	else
		return;
}

void ABaseCharacter::IncreaseMana()
{
	if (skillPoints > 0)
	{
		skillPoints--;
		UndoSkillPoints++;
		AllChoosenSkills.Add(ELastGivenSkill::Mana);
		currentMana += 4;
		manaGrowth++;
	}
	else
		return;
}

void ABaseCharacter::DecreaseMana()
{
	if (UndoSkillPoints != 0 && AllChoosenSkills.Last() == ELastGivenSkill::Mana)
	{
		UndoSkillPoints--;
		AllChoosenSkills.Pop();
		currentMana -= 4;
		manaGrowth--;
		skillPoints++;
	}
	else
		return;
}

void ABaseCharacter::EquipItem(AGearItem * Item)
{
	if (Item->Tags.Contains("Weapon"))
	{
		if (Gear[0])
		{
			Gear[0]->RemoveStats();
			inventory.Add(Gear[0]);
		}
		Item->GiveStats();
		Gear[0] = Item;
		inventory.RemoveSingle(Item);
	}
	else if (Item->Tags.Contains("Shield"))
	{
		if (Gear[1])
		{
			Gear[1]->RemoveStats();
			inventory.Add(Gear[1]);
		}
		Item->GiveStats();
		Gear[1] = Item;
		inventory.RemoveSingle(Item);
	}
	else if (Item->Tags.Contains("Helmet"))
	{
		if (Gear[2])
		{
			Gear[2]->RemoveStats();
			inventory.Add(Gear[2]);
		}
		Item->GiveStats();
		Gear[2] = Item;
		inventory.RemoveSingle(Item);
	}
	else if (Item->Tags.Contains("Breast"))
	{
		if (Gear[3])
		{
			Gear[3]->RemoveStats();
			inventory.Add(Gear[3]);
		}
		Item->GiveStats();
		Gear[3] = Item;
		inventory.RemoveSingle(Item);
	}
	else if (Item->Tags.Contains("Pants"))
	{
		if (Gear[4])
		{
			Gear[4]->RemoveStats();
			inventory.Add(Gear[4]);
		}
		Item->GiveStats();
		Gear[4] = Item;
		inventory.RemoveSingle(Item);
	}
	else if (Item->Tags.Contains("Boots"))
	{
		if (Gear[5])
		{
			Gear[5]->RemoveStats();
			inventory.Add(Gear[5]);
		}
		Item->GiveStats();
		Gear[5] = Item;
		inventory.RemoveSingle(Item);
	}
	else if (Item->Tags.Contains("Ring"))
	{
		if (!Gear[6])
		{
			Item->GiveStats();
			Gear[6] = Item;
			inventory.RemoveSingle(Item);
		}
		else if (!Gear[7])
		{
			Gear[7] = Item;
			Item->GiveStats();
			inventory.RemoveSingle(Item);
		}
		else
		{
			Gear[6]->RemoveStats();
			inventory.Add(Gear[6]);
			Item->GiveStats();
			Gear[6] = Item;
			inventory.RemoveSingle(Item);
		}
	}
}

void ABaseCharacter::UnequipItem(int slot)
{
	if (!Gear[slot])
		return;
	if (slot >= 8)
		return;
	if (inventory.Num() >= 50)
		return;

	Gear[slot]->RemoveStats();
	inventory.Add(Gear[slot]);
	Gear[slot] = NULL;
}

int ABaseCharacter::GetMoney()
{
	return money;
}

void ABaseCharacter::SetMoney(int money)
{
	this->money = money;
}

void ABaseCharacter::AddMoney(int addedMoney)
{
	money += addedMoney;
}

int ABaseCharacter::GetAmmunition()
{
	return ammunition;
}

void ABaseCharacter::SetAmmunition(int ammo)
{
	this->ammunition = ammo;
}

void ABaseCharacter::AddAmmunition(int addedAmmunition)
{
	ammunition += addedAmmunition;
}

void ABaseCharacter::PickUpItem(APickUp* item)
{
	if (!item->IsAttackable())
	{
		item->bPickedUp = true;
		inventory.Emplace(item);
	}
}

void ABaseCharacter::EmptyInventory()
{
	inventory.Empty();
}

bool ABaseCharacter::StillSpaceInInventory(int neededSpace)
{
	if (inventory.Num() + neededSpace <= inventorySpace)
		return true;
	return false;
}

TArray<APickUp*> ABaseCharacter::GetInventory()
{
	return inventory;
}

void ABaseCharacter::SetInventory(TArray<APickUp*> inventory)
{
	this->inventory = inventory;
}

void ABaseCharacter::AddToInventory(APickUp * item)
{
	inventory.Add(item);
}

void ABaseCharacter::RemoveFromInventory(APickUp* item)
{
	if (item->GetClass()->IsChildOf(APickUp::StaticClass()))
		if (!item->bIsQuestRelevant)
			inventory.RemoveSingle(item);
}

int ABaseCharacter::GetInventorySpace()
{
	return inventorySpace;
}

void ABaseCharacter::SetInventorySpace(int space)
{
	this->inventorySpace = space;
}

TArray<AGearItem*> ABaseCharacter::GetGear()
{
	return Gear;
}

void ABaseCharacter::Idle()
{
	ABaseCharacter* character = (ABaseCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);

	FVector distance = (FVector)character->GetActorLocation() - this->GetActorLocation();

	if (distance.Size() < PerceptionRange)
	{
		State = 1;
	}
}

void ABaseCharacter::Seek()
{
	ABaseCharacter* character = (ABaseCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);

	if (bShallFollow)
		FollowCharacter(character);

	FVector distance = (FVector)character->GetActorLocation() - this->GetActorLocation();

	if (distance.Size() > PerceptionRange)
		State = 0;
	else if (distance.Size() < AttackRange)
	{
		State = 2;
		MoveTo(this->GetActorLocation());
	}
}

void ABaseCharacter::Attack()
{
	ABaseCharacter* character = (ABaseCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);

	FVector distance = (FVector)character->GetActorLocation() - this->GetActorLocation();

	if (character->bAlive)
		BeginCombat(character);

	if (distance.Size() > AttackRange)
		State = 1;
}

void ABaseCharacter::Dead()
{
	return;
}

int ABaseCharacter::GetKarma()
{
	return karma;
}

void ABaseCharacter::SetKarma(int karma)
{
	this->karma = karma;
}

int ABaseCharacter::GetAlignment()
{
	return alignment;
}

void ABaseCharacter::SetAlignment(int value)
{
	this->alignment = value;
}

int ABaseCharacter::GetArmor()
{
	return armor;
}

void ABaseCharacter::SetArmor(int armor)
{
	this->armor = armor;
}

void ABaseCharacter::AddArmor(int value)
{
	armor += value;
}

int ABaseCharacter::GetDodge()
{
	return dodge;
}

void ABaseCharacter::SetDodge(int dodge)
{
	this->dodge = dodge;
}

int ABaseCharacter::GetMeele()
{
	return meele;
}

void ABaseCharacter::SetMeele(int meele)
{
	this->meele = meele;
}

int ABaseCharacter::GetBow()
{
	return bow;
}

void ABaseCharacter::SetBow(int bow)
{
	this->bow = bow;
}

int ABaseCharacter::GetProwling()
{
	return prowling;
}

void ABaseCharacter::SetProwling(int value)
{
	this->prowling = prowling;
}

int ABaseCharacter::GetPickPocket()
{
	return pickPocket;
}

void ABaseCharacter::SetPickPocket(int pick)
{
	this->pickPocket = pick;
}

int ABaseCharacter::GetHaggle()
{
	return haggle;
}

void ABaseCharacter::SetHaggle(int haggle)
{
	this->haggle = haggle;
}

int ABaseCharacter::GetPersuation()
{
	return persuation;
}

void ABaseCharacter::SetPersuation(int value)
{
	this->persuation = value;
}

int ABaseCharacter::GetPickLocks()
{
	return pickLocks;
}

void ABaseCharacter::SetPickLocks(int value)
{
	this->pickLocks = value;
}

void ABaseCharacter::ResetUndoSkillPoints()
{
	UndoSkillPoints = 0;
	AllChoosenSkills.Empty();
}

void ABaseCharacter::GiveXP(int amount)
{
	xp += amount;
}

int ABaseCharacter::ReturnExpToNextLevel()
{
	float changingVariable = 100;

	for (size_t i = 1; i < level; i++)
	{
		if (i <= 10)
			changingVariable *= 1.4;
		else if (i <= 15)
			changingVariable *= 1.25;
		else if (i <= 20)
			changingVariable *= 1.15;
		else if (i >= 30)
			changingVariable *= 1.1;
	}
	return changingVariable - xp;
}

void ABaseCharacter::CheckLevel()
{
	if (ReturnExpToNextLevel() <= 0)
		LevelUp();
}

void ABaseCharacter::LevelUp()
{
	xp = 0;
	level += 1;
	currentHealth += 2;
	currentMana += 2;
	if (level % 5 == 0)
		skillPoints += 2;
	else
		skillPoints += 1;
}

float ABaseCharacter::GetAttackCooldown()
{
	attackCooldown = 1.1f * UKismetMathLibrary::MultiplyMultiply_FloatFloat(0.98f, WeaponAttackspeed * (dexterity + (meele * 5)));

	return attackCooldown * 1 + AttackSpeedDebuff;
}

void ABaseCharacter::SetAttackCooldown(float value)
{
	this->attackCooldown = value;
}

void ABaseCharacter::SetShallAttack(bool shallAttack)
{
	bShallAttack = shallAttack;
}

bool ABaseCharacter::GetShallAttack()
{
	bInCombat = !bInCombat;
	return bShallAttack;
}

void ABaseCharacter::MoveTo(const FVector destination)
{
	AArcanumAIController* controller = (AArcanumAIController*)this->GetController();

	if (!controller)
		return;
	APawn* const Pawn = controller->GetPawn();

	if (Pawn)
	{
		UNavigationSystem* const NavSys = GetWorld()->GetNavigationSystem();
		float const Distance = FVector::Dist(destination, Pawn->GetActorLocation());

		// We need to issue move command only if far enough in order for walk animation to play correctly
		if (NavSys && (Distance > 120.0f))
		{
			NavSys->SimpleMoveToLocation(controller, destination);
		}
	}
}

int ABaseCharacter::CalculateSpellLimit()
{
	spellLimit = intelligence / 4;

	return spellLimit;
}

int ABaseCharacter::GetSpellLimit()
{
	return spellLimit;
}

void ABaseCharacter::SetSpellLimit(int limit)
{
	this->spellLimit = limit;
}

int ABaseCharacter::GetDamageValue()
{
	int damage = DamageModifier * (strength * (1 + (meele * 2 / 100)));

	return damage;
}

bool ABaseCharacter::CanAttackAlready()
{
	if (timeSinceLastAttack > GetAttackCooldown())
	{
		timeSinceLastAttack = 0;
		return true;
	}
	else
		return false;
}

void ABaseCharacter::ApplyDamage(ABaseCharacter* target, bool sourceIsPlayer)
{
	if (CanAttackAlready())
	{
		int damage = this->GetDamageValue();
		FString s = FString::FromInt(damage);
		FString answer = "";

		if (this->bAlive && target->bAlive)
		{
			OnAttack(target);
			if (!sourceIsPlayer)
			{
				target->bShallAttack = true;
				target->bInCombat = true;
				if (bShallAttack)
				{
					if (target->currentHealth - damage > 0)
					{
						target->AddToCurrentHealth(-damage);
						answer = (this->name + " hit " + target->name + " for " + s + " damage");
					}
					else
					{
						answer = (target->name + " ist gestorben :(");
						bInCombat = false;
						target->Death();
					}
				}
			}
			else
			{
				if (bShallAttack)
				{
					if (target->currentHealth - damage > 0)
					{
						target->AddToCurrentHealth(-damage);
						answer = (this->name + " hit " + target->name + " for " + s + " damage");
					}
					else
					{
						answer = (target->name + " ist gestorben :(");
						bInCombat = false;
						target->Death();
					}
				}
			}
			SetLogMessage(answer);
		}
	}
}

void ABaseCharacter::ApplyDamageToItem(APickUp* target, bool sourceIsPlayer)
{
	if (CanAttackAlready())
	{
		int damage = this->GetDamageValue();
		FString s = FString::FromInt(damage);
		FString answer = "";

		if (target->currentHealth - damage > 0)
		{
			target->currentHealth -= damage;
			answer = (this->name + " hit " + target->ToolTip + "for " + s + " damage");
		}
		else
		{
			answer = (target->LogText);
			bInCombat = false;
			target->Destroy();
		}
		SetLogMessage(answer);
	}
}

void ABaseCharacter::Death()
{
	SetActorEnableCollision(false);
	SetActorTickEnabled(false);

	State = 3;
	currentHealth = 0;
	bAlive = false;
	bInCombat = false;
}

void ABaseCharacter::SetLogMessage(FString text)
{
	currentLogMessage = text;
}

UTexture2D* ABaseCharacter::GetIcon()
{
	return icon;
}

void ABaseCharacter::SetIcon(UTexture2D * icon)
{
	this->icon = icon;
}

void ABaseCharacter::SetSex(FString sex)
{
	this->sex = sex;
}

bool ABaseCharacter::CheckAlive()
{
	return bAlive;
}

int ABaseCharacter::GetRewardEXP()
{
	return rewardExp;
}

void ABaseCharacter::SetRewardEXP(int value)
{
	this->rewardExp = value;
}

FString ABaseCharacter::GetLog()
{
	return currentLogMessage;
}

void ABaseCharacter::FollowCharacter(ABaseCharacter* character)
{
	FVector playerPosition = character->GetActorLocation();

	FVector distance = playerPosition - (FVector)this->GetActorLocation();

	MoveTo(playerPosition);
}

void ABaseCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
}

void ABaseCharacter::OnAttack(ABaseCharacter * target)
{
	FVector location = GetActorLocation();
	FVector targetLocation = target->GetActorLocation();
	FRotator rotation = UKismetMathLibrary::FindLookAtRotation(location, targetLocation);

	GetCapsuleComponent()->SetWorldRotation(rotation);

	bHasAttackStarted = true;
}

void ABaseCharacter::BeginCombat(ABaseCharacter* combatTarget)
{
	if (bPlayer)
	{
		ApplyDamage(combatTarget, true);
		combatTarget->ApplyDamage(this, false);
	}
	else
	{
		ApplyDamage(combatTarget, false);
		combatTarget->ApplyDamage(this, true);
	}

	bInCombat = true;
}