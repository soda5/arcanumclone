// Mischa Ahi / Philip Gandy @ 2016

#pragma once

#include "Magic.h"
#include "Magic_Fire_4.generated.h"

UCLASS()
class ARCANUMCLONE_API UMagic_Fire_4 : public UMagic
{
	GENERATED_BODY()
	
public:

	UMagic_Fire_4();

	UFUNCTION()
		virtual void Costs(UWorld* world) override;

	UFUNCTION()
		virtual void ActivateSpell(ABaseCharacter* target, UWorld* world) override;
	
};