// Mischa Ahi / Philip Gandy @ 2016

#include "ArcanumClone.h"
#include "SpellBook.h"

USpellBook::USpellBook()
{
	InitSpells();
}

void USpellBook::InitSpells()
{
	InitSpellAir1();
	InitSpellAir2();
	InitSpellAir3();
	InitSpellAir4();
	InitSpellEarth1();
	InitSpellEarth2();
	InitSpellEarth3();
	InitSpellEarth4();
	InitSpellWater1();
	InitSpellWater2();
	InitSpellWater3();
	InitSpellWater4();
	InitSpellFire1();
	InitSpellFire2();
	InitSpellFire3();
	InitSpellFire4();
}

void USpellBook::InitSpellAir1()
{
	UMagic_Air_1* air1 = NewObject<UMagic_Air_1>();

	air1->Name = "Vitality";
	air1->Description = "Fresh air will boost your physical vitality";
	Spells.Add(air1);
}

void USpellBook::InitSpellAir2()
{
	UMagic_Air_2* air2 = NewObject<UMagic_Air_2>();

	air2->Name = "Push";
	air2->Description = "Push an enemie away!";
	Spells.Add(air2);
}

void USpellBook::InitSpellAir3()
{
	UMagic_Air_3* air3 = NewObject<UMagic_Air_3>();

	air3->Name = "Mighty Push";
	air3->Description = "Push all enemies away!";
	Spells.Add(air3);
}

void USpellBook::InitSpellAir4()
{
	UMagic_Air_4* air4 = NewObject<UMagic_Air_4>();

	air4->Name = "Body of Air";
	air4->Description = "Morph your body to become an airy Guy!";
	Spells.Add(air4);
}

void USpellBook::InitSpellEarth1()
{
	UMagic_Earth_1* earth1 = NewObject<UMagic_Earth_1>();

	earth1->Name = "Stamina like earth";
	earth1->Description = "Your body will be strong like earth!";
	Spells.Add(earth1);
}

void USpellBook::InitSpellEarth2()
{
	UMagic_Earth_2* earth2 = NewObject<UMagic_Earth_2>();

	earth2->Name = "Earth Root";
	earth2->Description = "Make your enemy immobile!";
	Spells.Add(earth2);
}

void USpellBook::InitSpellEarth3()
{
	UMagic_Earth_3* earth3 = NewObject<UMagic_Earth_3>();

	earth3->Name = "Mass Root";
	earth3->Description = "Make all enemies tremble in fear!";
	Spells.Add(earth3);
}

void USpellBook::InitSpellEarth4()
{
	UMagic_Earth_4* earth4 = NewObject<UMagic_Earth_4>();

	earth4->Name = "Body of Earth";
	earth4->Description = "Morph your body to become an strong rooted rock!";
	Spells.Add(earth4);
}

void USpellBook::InitSpellWater1()
{
	UMagic_Water_1* water1 = NewObject<UMagic_Water_1>();

	water1->Name = "Beauty like water";
	water1->Description = "Wash your face to become an amazing beauty!";
	Spells.Add(water1);
}

void USpellBook::InitSpellWater2()
{
	UMagic_Water_2* water2 = NewObject<UMagic_Water_2>();

	water2->Name = "Wade in water";
	water2->Description = "Water will slow his attacking!";
	Spells.Add(water2);
}

void USpellBook::InitSpellWater3()
{
	UMagic_Water_3* water3 = NewObject<UMagic_Water_3>();

	water3->Name = "Mass Wade";
	water3->Description = "Your enemies wont hit you once!";
	Spells.Add(water3);
}

void USpellBook::InitSpellWater4()
{
	UMagic_Water_4* water4 = NewObject<UMagic_Water_4>();

	water4->Name = "Be water my friend";
	water4->Description = "Become Morphling!";
	Spells.Add(water4);
}

void USpellBook::InitSpellFire1()
{
	UMagic_Fire_1* fire1 = NewObject<UMagic_Fire_1>();

	fire1->Name = "Fire strength";
	fire1->Description = "Enchant your Fist to pack a punch!";
	Spells.Add(fire1);
}

void USpellBook::InitSpellFire2()
{
	UMagic_Fire_2* fire2 = NewObject<UMagic_Fire_2>();

	fire2->Name = "Burn bitch";
	fire2->Description = "Burn your goddamned enemie!";
	Spells.Add(fire2);
}

void USpellBook::InitSpellFire3()
{
	UMagic_Fire_3* fire3 = NewObject<UMagic_Fire_3>();

	fire3->Name = "Burn bitches";
	fire3->Description = "Make a living Hell 4 your enemies!";
	Spells.Add(fire3);
}

void USpellBook::InitSpellFire4()
{
	UMagic_Fire_4* fire4 = NewObject<UMagic_Fire_4>();

	fire4->Name = "EL DIAVOLO";
	fire4->Description = "Become the Devil himself";
	Spells.Add(fire4);
}