// Mischa Ahi / Philip Gandy @ 2016

#include "ArcanumClone.h"
#include "BreastPlateItem.h"


ABreastPlateItem::ABreastPlateItem()
{
	Name = "BreastPlateItem";

	Tags.Add("Breast");
}

void ABreastPlateItem::Use()
{
	AArcanumCloneCharacter* playerCharacter = (AArcanumCloneCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	playerCharacter->EquipItem(this);
}

void ABreastPlateItem::GiveStats()
{
	AArcanumCloneCharacter* playerCharacter = (AArcanumCloneCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	playerCharacter->AddArmor(armorvalue);
}

void ABreastPlateItem::RemoveStats()
{
	AArcanumCloneCharacter* playerCharacter = (AArcanumCloneCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	playerCharacter->AddArmor(-armorvalue);
}