// Mischa Ahi / Philip Gandy @ 2016

#include "ArcanumClone.h"
#include "Magic_Earth_3.h"
#include "BaseCharacter.h"

UMagic_Earth_3::UMagic_Earth_3()
{
	ManaCosts = 20;
	bIsInstant = true;
}

void UMagic_Earth_3::Costs(UWorld* world)
{
	Super::Costs(world);

	if (world)
	{
		APlayerController* controller = world->GetFirstPlayerController();

		if (controller)
		{
			ACharacter* character = controller->GetCharacter();
			if (character)
			{
				ABaseCharacter* playerCharacter = dynamic_cast<ABaseCharacter*>(character);
				if (playerCharacter->GetCurrentMana() >= ManaCosts)
				{
					playerCharacter->SetCurrentMana(-ManaCosts);
					bManaCostsHasBeenPayed = true;
				}
				else
					bManaCostsHasBeenPayed = false;
			}
		}
	}
}

void UMagic_Earth_3::ActivateSpell(ABaseCharacter * target, UWorld * world)
{
	Super::ActivateSpell(target, world);

	if (world)
	{
		APlayerController* controller = world->GetFirstPlayerController();

		if (controller)
		{
			ACharacter* character = controller->GetCharacter();
			if (character)
			{
				ABaseCharacter* playerCharacter = dynamic_cast<ABaseCharacter*>(character);

				if (bManaCostsHasBeenPayed)
				{
					for (TActorIterator<ABaseCharacter> ActorItr(world); ActorItr; ++ActorItr)
					{
						ABaseCharacter* Character = *ActorItr;

						FVector playerPosition = playerCharacter->GetActorLocation();
						FVector targetPosition = Character->GetActorLocation();

						FVector vector = playerPosition - targetPosition;

						float length = vector.Dist(playerPosition, targetPosition);

						if (length <= 500)
						{
							if (!Character->bPlayer)
							{
								Character->MovementSpeedSlowValue = 10;
								Character->MovementSlowDuration = 3.0f;
							}
						}
					}
				}
			}
		}
	}
}