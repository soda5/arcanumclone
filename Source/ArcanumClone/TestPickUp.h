// Mischa Ahi @ 2016

#pragma once

#include "PickUp.h"
#include "TestPickUp.generated.h"

UCLASS()
class ARCANUMCLONE_API ATestPickUp : public APickUp
{
	GENERATED_BODY()
	
public:

	ATestPickUp();

	void PickedUp() override;
	
};