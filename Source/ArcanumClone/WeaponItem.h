// Mischa Ahi / Philip Gandy @ 2016

#pragma once

#include "GearItem.h"
#include "WeaponItem.generated.h"

UCLASS()
class ARCANUMCLONE_API AWeaponItem : public AGearItem
{
	GENERATED_BODY()
	
public:
	AWeaponItem();

	void Use() override;

	void GiveStats() override;

	void RemoveStats() override;
	
	void Sell(AArcanumCloneCharacter* player);

protected:

	float damageModifier;

	float attackspeed;
	
};