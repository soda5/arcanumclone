// Mischa Ahi / Philip Gandy @ 2016

#pragma once

#include "BaseNPC.h"
#include "Merchant.generated.h"

UCLASS()
class ARCANUMCLONE_API AMerchant : public ABaseNPC
{
	GENERATED_BODY()
	
public:

	AMerchant();

	void BeginPlay() override;

private:

	void Deactivate(APickUp* pickUp);
};