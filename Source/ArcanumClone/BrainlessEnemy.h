// Mischa Ahi / Philip Gandy @ 2016

#pragma once

#include "BaseCharacter.h"
#include "ArcanumAIController.h"
#include "PickUp.h"
#include "BrainlessEnemy.generated.h"

UCLASS()
class ARCANUMCLONE_API ABrainlessEnemy : public ABaseCharacter
{
	GENERATED_BODY()

public:

	ABrainlessEnemy();

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	virtual int GetDamageValue() override;

	UFUNCTION(BlueprintCallable, Category = "Loot")
	virtual void SpawnLoot();


	virtual void Death() override;

	UPROPERTY(EditAnywhere)
		class UPawnSensingComponent* PawnSensingComponent;

	UPROPERTY(EditAnywhere)
		int damage;

protected:

	UPROPERTY(EditAnywhere)
		TArray<APickUp*> lootTable;
	
};