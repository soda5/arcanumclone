// Mischa Ahi / Philip Gandy @ 2016

#pragma once

#include "GearItem.h"
#include "HelmetItem.generated.h"

UCLASS()
class ARCANUMCLONE_API AHelmetItem : public AGearItem
{
	GENERATED_BODY()
	
public:
	AHelmetItem();

	void Use() override;

	void GiveStats() override;

	void RemoveStats() override;

protected:
	
	int armorvalue;
	
};