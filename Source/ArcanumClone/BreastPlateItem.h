// Mischa Ahi / Philip Gandy @ 2016

#pragma once

#include "GearItem.h"
#include "BreastPlateItem.generated.h"

/**
 * 
 */
UCLASS()
class ARCANUMCLONE_API ABreastPlateItem : public AGearItem
{
	GENERATED_BODY()
	
public:
	ABreastPlateItem();

	void Use() override;

	void GiveStats() override;

	void RemoveStats() override;

protected:
	
	int armorvalue;

};