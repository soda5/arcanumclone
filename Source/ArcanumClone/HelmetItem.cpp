// Mischa Ahi / Philip Gandy @ 2016

#include "ArcanumClone.h"
#include "HelmetItem.h"

AHelmetItem::AHelmetItem()
{
	Name = "HelmetItem";

	Tags.Add("Helmet");
	ToolTip = "Mysterious Helmet of Retep \n\nThis Helmet looks like an ancient treasure, it is surrounded by a strange magnetic aura, no matter how hard you try, you can't touch this \nHelmet with your sword. It seems like the Owner of this Helmet will be secure from metal weapons. \nOn the side is a little scratchmark which shows the Initials 'LR'. \n\nArmor +2";
	armorvalue = 2;
}

void AHelmetItem::Use()
{
	AArcanumCloneCharacter* playerCharacter = (AArcanumCloneCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	playerCharacter->EquipItem(this);
}

void AHelmetItem::GiveStats()
{
	AArcanumCloneCharacter* playerCharacter = (AArcanumCloneCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	playerCharacter->AddArmor(armorvalue);
}

void AHelmetItem::RemoveStats()
{
	AArcanumCloneCharacter* playerCharacter = (AArcanumCloneCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	playerCharacter->AddArmor(-armorvalue);
}