// Mischa Ahi / Philip Gandy @ 2016
#pragma once
#include "GameFramework/Character.h"
#include "BaseCharacter.h"
#include "PlayerSaveValues.h"
#include "ArcanumCloneGameMode.h"
#include "ArcanumCloneCharacter.generated.h"

UCLASS(Blueprintable)
class AArcanumCloneCharacter : public ABaseCharacter
{
	GENERATED_BODY()

public:
	AArcanumCloneCharacter();

	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION()
		void OnCasting(ABaseCharacter* target);

	UFUNCTION()
		void OnCastingAOE();

	UFUNCTION(BlueprintCallable, Category = "Magic")
		bool CanLearnSpell(UMagic* magic);

	UFUNCTION(BlueprintCallable, Category = "SaveGame")
		void SaveGame();

	UFUNCTION(BlueprintCallable, Category = "SaveGame")
		void SaveGameToSlot(FString slotName);

	UFUNCTION(BlueprintCallable, Category = "LevelChange")
		void LoadLevelChange();

	UFUNCTION(BlueprintCallable, Category = "LoadGame")
		FString LoadGame();

	UFUNCTION(BlueprintCallable, Category = "LoadGame")
		void LoadGameFromSlot(FString slotName);

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	virtual void ApplyDamage(ABaseCharacter* target, bool sourceIsPlayer) override;

	UPROPERTY()
		float CastingTime = 2;

	UPROPERTY()
		float AOECastingTime;

private:

	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UDecalComponent* CursorToWorld;

};

