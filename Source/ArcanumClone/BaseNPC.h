// Mischa Ahi @ 2016

#pragma once

#include "BaseCharacter.h"
#include "ArcanumAIController.h"
#include "BaseNPC.generated.h"

UCLASS()
class ARCANUMCLONE_API ABaseNPC : public ABaseCharacter
{
	GENERATED_BODY()

public:
	ABaseNPC();

	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION(BlueprintPure, Category = "Dialogue")
		FString GetDialogue();

	UFUNCTION(BlueprintPure, Category = "Dialogue")
		TArray<FString> GetAnswers();

	UFUNCTION(BlueprintCallable, Category = "Dialogue")
		void StartDialogue();

	UFUNCTION(BlueprintPure, Category = "Dialogue")
		int GetDialoguePosition();

	UFUNCTION(BlueprintCallable, Category = "Dialogue")
		void SetDialoguePosition(int position);

	UFUNCTION(BlueprintCallable, Category = "Dialogue")
		virtual void SetText();

	UFUNCTION(BlueprintCallable, Category = "Dialogue")
		virtual int ReturnNextDialogPosition(int choosenAnswer);

	UFUNCTION(BlueprintPure, Category = "Dialogue")
		int GetNumberOfAnswers();

	UFUNCTION(BlueprintPure, Category = "Dialogue")
		bool IsThisTheEnd();

protected:

	bool bEndDialogHere;

	FString dialogueSentence;

	TArray<FString> dialogueOptions;

	int dialoguePosition = 0;

	int numberOfAnswers;

};