// Mischa Ahi / Philip Gandy @ 2016

#include "ArcanumClone.h"
#include "BrainlessEnemy.h"
#include "Perception/PawnSensingComponent.h"
#include "PickUp.h"
#include "MoneyPickUp.h"
#include "AmmunitionPickup.h"
#include "WeaponItem.h"
#include "Kismet/KismetMathLibrary.h"


ABrainlessEnemy::ABrainlessEnemy()
{
	PawnSensingComponent = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("PawnSensingComponent"));
	PawnSensingComponent->SetPeripheralVisionAngle(360.f);
	PawnSensingComponent->SightRadius = 0;
	rewardExp = 50;
	damage = 1;
	PerceptionRange = 300;

	bShallAttack = true;
	bShallFollow = true;
}

void ABrainlessEnemy::BeginPlay()
{
	Super::BeginPlay();

	PawnSensingComponent->SightRadius = PerceptionRange;

	State = 0;
}

void ABrainlessEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

int ABrainlessEnemy::GetDamageValue()
{
	return damage;
}

void ABrainlessEnemy::SpawnLoot()
{
	if(UWorld* const World = GetWorld())
	{
		int randomNum = UKismetMathLibrary::RandomIntegerInRange(1, 3);
		switch (randomNum)
		{
		case 1:
			if (MoneyBP)
			{
				AMoneyPickUp* LootItem = World->SpawnActor<AMoneyPickUp>(MoneyBP, GetActorLocation(), FRotator(0, 0, 0));

				LootItem->rewardMoney = UKismetMathLibrary::RandomIntegerInRange(12,28);
			}
			break;
		case 2:
			if (AmmoBP)
			{
				AAmmunitionPickUp* LootItem = World->SpawnActor<AAmmunitionPickUp>(AmmoBP, GetActorLocation(), FRotator(0, 0, 0));

				LootItem->rewardAmmunition = UKismetMathLibrary::RandomIntegerInRange(3,5);
			}
			break;
		case 3:
			if (WeaponBP)
				AWeaponItem* LootItem = World->SpawnActor<AWeaponItem>(WeaponBP, GetActorLocation(), FRotator(0, 0, 0));
			break;
		default:
			break;
		}
	}
}

void ABrainlessEnemy::Death()
{
	Super::Death();
	
	if (UKismetMathLibrary::RandomFloatInRange(1, 100) <= 20)
	{
		SpawnLoot();
	}
}