// Mischa Ahi / Philip Gandy @ 2016

#pragma once

#include "BaseNPC.h"
#include "ArcanumCloneCharacter.h"
#include "HealthPotion.h"
#include "MerchantsPendant.h"
#include "Priest_Welfin.generated.h"

/**
 *
 */
UCLASS()
class ARCANUMCLONE_API APriest_Welfin : public ABaseNPC
{
	GENERATED_BODY()
public:

	APriest_Welfin();

	virtual void SetText() override;

	virtual int ReturnNextDialogPosition(int choosenAnswer) override;

	UFUNCTION(BlueprintCallable, Category = "Dialog")
		void GetReferences();

	UPROPERTY(EditAnywhere)
		TSubclassOf<AHealthPotion> Healthpotion;

	UPROPERTY(EditAnywhere)
		TSubclassOf<AMerchantsPendant> QuestPendantReference;

private:

	AArcanumCloneCharacter* playerCharacter;

	UWorld* world;

	int ButtonCounter;

	AMerchantsPendant* Pendant;

	bool bRingWasChoosen = false;

	bool bMoneyWasChoosen = false;

	int amountOfHeealthpotions = 3;

	int extraAnswers;

};
