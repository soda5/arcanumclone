// Mischa Ahi / Philip Gandy @ 2016

#include "ArcanumClone.h"
#include "BootsItem.h"

ABootsItem::ABootsItem()
{
	Name = "BootsItem";

	Tags.Add("Boots");
	ToolTip = "Lightningboots of Retep \n\nThese legendary Boots are supposed to let the Owner outrun Lightning itself, but until now no one was able to unlock their frightening \nPower. Even thought you also can't enable their true potential, you still feel a lot lighter while wearing these Boots. \nIt woulb be a lot easier to Dodge with these Boots so you took them. \nOn each Boot is a letter, on the left an 'L' and on the right an 'R'. \n\narmor +2";
	armorvalue = 2;
}

void ABootsItem::Use()
{
	AArcanumCloneCharacter* playerCharacter = (AArcanumCloneCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	playerCharacter->EquipItem(this);
}

void ABootsItem::GiveStats()
{
	AArcanumCloneCharacter* playerCharacter = (AArcanumCloneCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	playerCharacter->AddArmor(armorvalue);
}

void ABootsItem::RemoveStats()
{
	AArcanumCloneCharacter* playerCharacter = (AArcanumCloneCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	playerCharacter->AddArmor(-armorvalue);
}