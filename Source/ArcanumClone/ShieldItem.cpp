// Mischa Ahi / Philip Gandy @ 2016

#include "ArcanumClone.h"
#include "ShieldItem.h"

AShieldItem::AShieldItem()
{
	Name = "ShieldItem";

	Tags.Add("Shield");
	ToolTip = "Shield of the Untouchable Retep \n\nThis Shield has marks and scratches all over it surface, but it doesn't look like it would break anytime soon. Some of the marks seem \nto be from heavy weapons like Greataxes or Greathammer, still the durability and sturdiness of this Shield seem to be infinite. \nThe only place untouched by combatmarks are the big letters 'LR' in the middle. \n\nArmor +3";
	armorvalue = 3;
}

void AShieldItem::Use()
{
	AArcanumCloneCharacter* playerCharacter = (AArcanumCloneCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	playerCharacter->EquipItem(this);
}

void AShieldItem::GiveStats()
{
	AArcanumCloneCharacter* playerCharacter = (AArcanumCloneCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	playerCharacter->AddArmor(armorvalue);
}

void AShieldItem::RemoveStats()
{
	AArcanumCloneCharacter* playerCharacter = (AArcanumCloneCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	playerCharacter->AddArmor(-armorvalue);
}