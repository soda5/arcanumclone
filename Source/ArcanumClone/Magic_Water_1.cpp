// Mischa Ahi / Philip Gandy @ 2016

#include "ArcanumClone.h"
#include "Magic_Water_1.h"
#include "BaseCharacter.h"

UMagic_Water_1::UMagic_Water_1()
{
	ManaCosts = 5;
}

void UMagic_Water_1::Costs(UWorld* world)
{
	Super::Costs(world);

	if (world)
	{
		APlayerController* controller = world->GetFirstPlayerController();

		if (controller)
		{
			ACharacter* character = controller->GetCharacter();
			if (character)
			{
				ABaseCharacter* playerCharacter = dynamic_cast<ABaseCharacter*>(character);
				if (playerCharacter->GetCurrentMana() >= ManaCosts)
				{
					playerCharacter->SetCurrentMana(-ManaCosts);
					bManaCostsHasBeenPayed = true;
				}
				else
					bManaCostsHasBeenPayed = false;
			}
		}
	}
}

void UMagic_Water_1::ActivateSpell(ABaseCharacter* target, UWorld* world)
{
	Super::ActivateSpell(target, world);

	if (world)
	{
		APlayerController* controller = world->GetFirstPlayerController();

		if (controller)
		{
			ACharacter* character = controller->GetCharacter();
			if (character)
			{
				ABaseCharacter* playerCharacter = dynamic_cast<ABaseCharacter*>(character);

				if (bManaCostsHasBeenPayed)
				{
					if (target)
					{
						target->IncreaseBeautyByMagic();
						target->IncreaseBeautyByMagic();
						target->IncreaseBeautyByMagic();
						target->IncreaseBeautyByMagic();
					}
				}
			}
		}
	}
}