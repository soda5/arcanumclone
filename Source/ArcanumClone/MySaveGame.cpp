// Mischa Ahi / Philip Gandy @ 2016

#include "ArcanumClone.h"
#include "MySaveGame.h"

void UMySaveGame::SaveCharacterValues(ABaseCharacter * character)
{
	FPlayerValues values = FPlayerValues();

	values.SavePlayerValues(character);

	PlayerValues.Add(values); // Saves the player values to the next position in values-array
}

void UMySaveGame::LoadCharacterValues(UWorld* const world, ABaseCharacter * character, int characterIndex)
{
	FPlayerValues values = FPlayerValues();

	values = PlayerValues[characterIndex];

	values.LoadPlayerValues(character);

	for (TActorIterator<AGearItem> ActorItr(world); ActorItr; ++ActorItr)
	{
		for (int i = 0; i < character->Gear.Num(); i++)
		{
			if (character->Gear[i] != nullptr)
			{
				if (ActorItr->Tags[0] == character->Gear[i]->Tags[0])
				{
					ActorItr->SetActorTransform(character->Gear[i]->GetActorTransform());
					ActorItr->bPickedUp = true;
					ActorItr->Mesh->SetSimulatePhysics(false);
					ActorItr->SetActorEnableCollision(false);
					ActorItr->SetActorHiddenInGame(true);
				}
			}
		}
	}
}

void UMySaveGame::SavePlayers(UWorld* const world)
{
	if (world)
	{
		for (TActorIterator<ABaseCharacter> ActorItr(world); ActorItr; ++ActorItr)
		{
			SaveCharacterValues(*ActorItr);
			LevelName = world->GetMapName();
		}
	}
}

void UMySaveGame::LoadPlayers(UWorld* const world)
{
	for (TActorIterator<ABaseCharacter> ActorItr(world); ActorItr; ++ActorItr) // Get all characters in current world and iterate
	{
		for (int i = 0; i < PlayerValues.Num(); i++) // Iterate for the numbers of objecs in PlayerValues array
		{
			if (ActorItr->NPCNumber == PlayerValues[i].NPCNumber) // If the world character equals the saved one
			{
				if (PlayerValues[i].bAlive)
					LoadCharacterValues(world, *ActorItr, i);
				else
					ActorItr->Death();
			}
		}
	}
}

void UMySaveGame::SavePlayerForLevelChange(ABaseCharacter * player)
{
	FPlayerValues values = FPlayerValues();

	values.SavePlayerValues(player);

	LevelChange = values;
}

void UMySaveGame::LoadPlayerFromLevelChange(ABaseCharacter * player, UWorld* world)
{
	FPlayerValues values = FPlayerValues();

	values = LevelChange;

	values.LoadPlayerValues(player);

	TArray<FString> inventoryStrings = LevelChange.inventoryStrings;

	player->EmptyInventory();

	for (int i = 0; i < inventoryStrings.Num(); i++)
	{
		FString names = inventoryStrings[i];

		if (names == "AwesomeRingOfBlasphemy")
		{
			AGearItem* blasphemyRing = world->SpawnActor<AAwesomeRingOfBlasphemy>(player->AwesomeRingOfBlasphemyBP, player->GetActorLocation(), FRotator(0, 0, 0));
			player->AddToInventory(blasphemyRing);
			Deactivate(blasphemyRing);
		}
		else if (names == "BootsItem")
		{
			AGearItem* boots = world->SpawnActor<ABootsItem>(player->BootsBP, player->GetActorLocation(), FRotator(0, 0, 0));
			player->AddToInventory(boots);
			Deactivate(boots);
		}
		else if (names == "BreastPlateItem")
		{
			AGearItem* breastPlate = world->SpawnActor<ABreastPlateItem>(player->BreastPlateBP, player->GetActorLocation(), FRotator(0, 0, 0));
			player->AddToInventory(breastPlate);
			Deactivate(breastPlate);
		}
		else if (names == "HelmetItem")
		{
			AGearItem* helmet = world->SpawnActor<AHelmetItem>(player->HelmetBP, player->GetActorLocation(), FRotator(0, 0, 0));
			player->AddToInventory(helmet);
			Deactivate(helmet);
		}
		else if (names == "PantsItem")
		{
			AGearItem* pants = world->SpawnActor<APantsItem>(player->PantsBP, player->GetActorLocation(), FRotator(0, 0, 0));
			player->AddToInventory(pants);
			Deactivate(pants);
		}
		else if (names == "RingItem")
		{
			AGearItem* ring = world->SpawnActor<ARingItem>(player->RingBP, player->GetActorLocation(), FRotator(0, 0, 0));
			player->AddToInventory(ring);
			Deactivate(ring);
		}
		else if (names == "ShieldItem")
		{
			AGearItem* shield = world->SpawnActor<AShieldItem>(player->ShieldBP, player->GetActorLocation(), FRotator(0, 0, 0));
			player->AddToInventory(shield);
			Deactivate(shield);
		}
		else if (names == "WeaponItem")
		{
			AGearItem* weapon = world->SpawnActor<AWeaponItem>(player->WeaponBP, player->GetActorLocation(), FRotator(0, 0, 0));
			player->AddToInventory(weapon);
			Deactivate(weapon);
		}
		else if (names == "HealthPotion")
		{
			AHealthPotion* potion = world->SpawnActor<AHealthPotion>(player->HPPotionBP, player->GetActorLocation(), FRotator(0, 0, 0));
			player->AddToInventory(potion);
			Deactivate(potion);
		}
		else if (names == "MerchantsPendant")
		{
			AMerchantsPendant* pendant = world->SpawnActor<AMerchantsPendant>(player->MerchantsPendantBP, player->GetActorLocation(), FRotator(0, 0, 0));
			player->AddToInventory(pendant);
			Deactivate(pendant);
		}
	}

	TArray<FString> gearStrings = LevelChange.GearStrings;

	for (int i = 0; i < gearStrings.Num(); i++)
	{
		FString names = gearStrings[i];

		if (names == "AwesomeRingOfBlasphemy")
		{
			AGearItem* blasphemyRing = world->SpawnActor<AAwesomeRingOfBlasphemy>(player->AwesomeRingOfBlasphemyBP, player->GetActorLocation(), FRotator(0, 0, 0));
			player->EquipItem(blasphemyRing);
			Deactivate(blasphemyRing);
		}
		else if (names == "BootsItem")
		{
			AGearItem* boots = world->SpawnActor<ABootsItem>(player->BootsBP, player->GetActorLocation(), FRotator(0, 0, 0));
			player->EquipItem(boots);
			Deactivate(boots);
		}
		else if (names == "BreastPlateItem")
		{
			AGearItem* breastPlate = world->SpawnActor<ABreastPlateItem>(player->BreastPlateBP, player->GetActorLocation(), FRotator(0, 0, 0));
			player->EquipItem(breastPlate);
			Deactivate(breastPlate);
		}
		else if (names == "HelmetItem")
		{
			AGearItem* helmet = world->SpawnActor<AHelmetItem>(player->HelmetBP, player->GetActorLocation(), FRotator(0, 0, 0));
			player->EquipItem(helmet);
			Deactivate(helmet);
		}
		else if (names == "PantsItem")
		{
			AGearItem* pants = world->SpawnActor<APantsItem>(player->PantsBP, player->GetActorLocation(), FRotator(0, 0, 0));
			player->EquipItem(pants);
			Deactivate(pants);
		}
		else if (names == "RingItem")
		{
			AGearItem* ring = world->SpawnActor<ARingItem>(player->RingBP, player->GetActorLocation(), FRotator(0, 0, 0));
			player->EquipItem(ring);
			Deactivate(ring);
		}
		else if (names == "ShieldItem")
		{
			AGearItem* shield = world->SpawnActor<AShieldItem>(player->ShieldBP, player->GetActorLocation(), FRotator(0, 0, 0));
			player->EquipItem(shield);
			Deactivate(shield);
		}
		else if (names == "WeaponItem")
		{
			AGearItem* weapon = world->SpawnActor<AWeaponItem>(player->WeaponBP, player->GetActorLocation(), FRotator(0, 0, 0));
			player->EquipItem(weapon);
			Deactivate(weapon);
		}
	}
}

void UMySaveGame::Deactivate(APickUp* pickUp)
{
	pickUp->Mesh->SetSimulatePhysics(false);
	pickUp->SetActorEnableCollision(false);
	pickUp->SetActorHiddenInGame(true);
}