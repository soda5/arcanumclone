// Mischa Ahi / Philip Gandy @ 2016

#pragma once

#include "Magic.h"
#include "Magic_Air_1.generated.h"


UCLASS()
class ARCANUMCLONE_API UMagic_Air_1 : public UMagic
{
	GENERATED_BODY()

public:

	UMagic_Air_1();

	UFUNCTION()
		virtual void Costs(UWorld* world) override;

	UFUNCTION()
		virtual void ActivateSpell(ABaseCharacter* target, UWorld* world) override;
};
