// Mischa Ahi / Philip Gandy @ 2016

#pragma once

#include "Magic.h"
#include "Magic_Water_3.generated.h"

UCLASS()
class ARCANUMCLONE_API UMagic_Water_3 : public UMagic
{
	GENERATED_BODY()
	
public:

	UMagic_Water_3();

	UFUNCTION()
		virtual void Costs(UWorld* world) override;

	UFUNCTION()
		virtual void ActivateSpell(ABaseCharacter* target, UWorld* world) override;
	
};