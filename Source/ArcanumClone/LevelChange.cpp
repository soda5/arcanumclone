// Mischa Ahi / Philip Gandy @ 2016

#include "ArcanumClone.h"
#include "LevelChange.h"


ALevelChange::ALevelChange()
{
	PrimaryActorTick.bCanEverTick = true;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
}

void ALevelChange::BeginPlay()
{
	Super::BeginPlay();
}

void ALevelChange::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}

void ALevelChange::Activate(FName levelName, AArcanumCloneCharacter* player)
{
	UWorld* world = GetWorld();
	if (world)
	{
		UGameInstance* instance = world->GetGameInstance();
		if (instance)
		{
			USwitchLevelValues* values = Cast<USwitchLevelValues>(instance);

			values->bChangedLevel = true;
			values->TargetLevel = levelName.ToString();

			UMySaveGame* saveGame = dynamic_cast<UMySaveGame*>(UGameplayStatics::CreateSaveGameObject(UMySaveGame::StaticClass()));

			UWorld* world = GetWorld();

			AArcanumCloneCharacter* character = (AArcanumCloneCharacter*)world->GetFirstPlayerController()->GetLocalPlayer();

			saveGame->SavePlayerForLevelChange(player);

			UGameplayStatics::SaveGameToSlot(saveGame, "LevelChangeSave", 0);

			UGameplayStatics::OpenLevel(GetWorld(), levelName, true, "");
		}
	}
}