// Mischa Ahi / Philip Gandy @ 2016

#pragma once

#include "PickUp.h"
#include "MoneyPickUp.generated.h"


UCLASS()
class ARCANUMCLONE_API AMoneyPickUp : public APickUp
{
	GENERATED_BODY()
	
public:

	AMoneyPickUp();

	void PickedUp() override;
	
	UPROPERTY(EditAnywhere)
		int rewardMoney;

};