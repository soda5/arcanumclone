// Mischa Ahi / Philip Gandy @ 2016

#include "ArcanumClone.h"
#include "WeaponItem.h"

AWeaponItem::AWeaponItem()
{
	Name = "WeaponItem";

	Tags.Add("Weapon");
	damageModifier = FMath::FRandRange(0.25,0.75);
	attackspeed = FMath::FRandRange(0.1,0.3);
	TArray<FStringFormatArg> args;
	args.Add(FStringFormatArg(damageModifier * 100));
	args.Add(FStringFormatArg(attackspeed * 100));
	ToolTip = FString::Format(TEXT("Sword of the allmighty Retep \n\nTis Sword has bathed in the Blood of many unfortunate Souls who stood in the Way of it's Owner. The Shine on the Blade is slightly red \nand sinister. You feel pitty for those Souls who fought against this Sword. \nThe Initials 'LR' are written on the Blade. \n\nDamage + {0}%\nAttackspeed + {1}%"), args);
}

void AWeaponItem::Use()
{
	AArcanumCloneCharacter* playerCharacter = (AArcanumCloneCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	playerCharacter->EquipItem(this);
}

void AWeaponItem::GiveStats()
{
	AArcanumCloneCharacter* playerCharacter = (AArcanumCloneCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	playerCharacter->WeaponAttackspeed += attackspeed;
	playerCharacter->DamageModifier += damageModifier;
}

void AWeaponItem::RemoveStats()
{
	AArcanumCloneCharacter* playerCharacter = (AArcanumCloneCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	playerCharacter->WeaponAttackspeed += -attackspeed;
	playerCharacter->DamageModifier += -damageModifier;
}