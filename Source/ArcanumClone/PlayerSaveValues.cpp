// Mischa Ahi / Philip Gandy @ 2016

#include "ArcanumClone.h"
#include "PlayerSaveValues.h"

void FPlayerValues::SavePlayerValues(ABaseCharacter* character)
{
	bHelmetVisible = character->Helmet->IsVisible();
	transform = character->GetActorTransform();
	NPCNumber = character->NPCNumber;
	Gear = character->Gear;
	BonusMovementSpeed = character->BonusMovementSpeed;
	bAlive = character->bAlive;
	WeaponAttackspeed = character->WeaponAttackspeed;
	DamageModifier = character->DamageModifier;
	rewardExp = character->GetRewardEXP();
	Icon = character->GetIcon();
	name = character->GetName();
	race = character->GetRace();
	sex = character->GetSex();
	skillPoints = character->GetSkillPoints();
	xp = character->GetXp();
	healthGrowth = character->GetHealthGrowth();
	manaGrowth = character->GetManaGrowth();
	money = character->GetMoney();
	ammunition = character->GetAmmunition();
	strength = character->GetStrength();
	intelligence = character->GetIntelligence();
	stamina = character->GetStamina();
	willPower = character->GetWillPower();
	dexterity = character->GetDexterity();
	perception = character->GetPerception();
	beauty = character->GetBeauty();
	charisma = character->GetCharisma();
	maxHealth = character->GetMaxHealth();
	currentHealth = character->GetCurrentHealth();
	maxMana = character->GetMaxMana();
	armor = character->GetArmor();
	dodge = character->GetDodge();
	meele = character->GetMeele();
	prowling = character->GetProwling();
	haggle = character->GetHaggle();
	persuation = character->GetPersuation();
	pickLocks = character->GetPickLocks();
	carryWeight = character->GetCarryWeight();
	movementSpeed = character->GetMovementSpeed();
	spellLimit = character->CalculateSpellLimit();
	karma = character->GetKarma();
	inventory = character->GetInventory();
	inventorySpace = character->GetInventorySpace();
	alignment = character->GetAlignment();
	attackCooldown = character->GetAttackCooldown();
	bStartedAttackCaveGoblins = character->bStartedAttackCaveGoblins;
	bStartedMerchantsPendant = character->bStartedMerchantsPendant;
	bFinishedAttackCaveGoblinsObjective = character->bFinishedAttackCaveGoblinsObjective;
	bFoundMerchantsPendant = character->bFoundMerchantsPendant;
	bTalkedWithThePriest = character->bTalkedWithThePriest;
	for (int i = 0; i < Gear.Num(); i++)
	{
		if (Gear[i])
		{
			GearStrings.Add(Gear[i]->Name);
		}
	}
	for (int i = 0; i < inventory.Num(); i++)
	{
		inventoryStrings.Add(inventory[i]->Name);
	}

	if (character->bPlayer)
	{
		for (int i = 0; i < 16; i++)
		{
			LearnedSpells.Add(character->SpellBook->Spells[i]->bIsLearned);
		}
	}
}

void FPlayerValues::LoadPlayerValues(ABaseCharacter * character)
{
	character->Helmet->SetVisibility(bHelmetVisible);
	character->SetActorTransform(transform);
	character->Gear = Gear;
	character->BonusMovementSpeed = BonusMovementSpeed;
	character->bAlive = bAlive;
	character->WeaponAttackspeed = WeaponAttackspeed;
	character->DamageModifier = DamageModifier;
	character->SetRewardEXP(rewardExp);
	character->SetIcon(Icon);
	character->SetName(name);
	character->SetRace(race);
	character->SetSex(sex);
	character->SetSkillPoints(skillPoints);
	character->SetXp(xp);
	character->SetHealthGrowth(healthGrowth);
	character->SetManaGrowth(manaGrowth);
	character->SetMoney(money);
	character->SetAmmunition(ammunition);
	character->SetStrength(strength);
	character->SetIntelligence(intelligence);
	character->SetStamina(stamina);
	character->SetWillPower(willPower);
	character->SetDexterity(dexterity);
	character->SetPerception(perception);
	character->SetBeauty(beauty);
	character->SetCharisma(charisma);
	character->SetMaxHealth(maxHealth);
	character->SetCurrentHealth(currentHealth);
	character->SetMaxMana(maxMana);
	character->SetArmor(armor);
	character->SetBow(bow);
	character->SetDodge(dodge);
	character->SetMeele(meele);
	character->SetPickPocket(pickPocket);
	character->SetProwling(prowling);
	character->SetHaggle(haggle);
	character->SetPersuation(persuation);
	character->SetPickLocks(pickLocks);
	character->SetCarryWeight(carryWeight);
	character->SetMovementSpeed(movementSpeed);
	character->SetSpellLimit(spellLimit);
	character->SetKarma(karma);
	character->SetInventory(inventory);
	character->SetInventorySpace(inventorySpace);
	character->SetAlignment(alignment);
	character->SetAttackCooldown(attackCooldown);
	character->bStartedAttackCaveGoblins = bStartedAttackCaveGoblins;
	character->bStartedMerchantsPendant = bStartedMerchantsPendant;
	character->bFinishedAttackCaveGoblinsObjective = bFinishedAttackCaveGoblinsObjective;
	character->bFoundMerchantsPendant = bFoundMerchantsPendant;
	character->bTalkedWithThePriest = bTalkedWithThePriest;

	if (character->bPlayer)
	{
		for (int i = 0; i < 16; i++)
		{
			character->SpellBook->Spells[i]->bIsLearned = LearnedSpells[i];
		}
	}
}