// Mischa Ahi / Philip Gandy @ 2016

#include "ArcanumClone.h"
#include "ArcanumCloneCharacter.h"
#include "MySaveGame.h"
#include "Kismet/KismetMathLibrary.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"

AArcanumCloneCharacter::AArcanumCloneCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->bAbsoluteRotation = true; // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 1000.f;
	CameraBoom->RelativeRotation = FRotator(-60.f, 0.f, 0.f);
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm



	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/TopDownCPP/Blueprints/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	NPCNumber = 0;

	currentLogMessage = "";

	name = "Player";
	race = "Human";
	sex = "Awesome!";

	bStateMachineActive = false; // stateMachine is just for NPC's
	bPlayer = true;
	bShallAttack = false;
}

void AArcanumCloneCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (CursorToWorld != nullptr)
	{
		if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}
	//Timer to make sure that the cast animation finishes
	if (CastingTime > 0)
	{
		if (CastingTime - deltaTime < 0)
			CastingTime = 0;
		else
			CastingTime -= deltaTime;
	}
	if (CastingTime == 0)
		bIsCasting = false;

	//Timer to make sure that the AOE cast animation finishes
	if (AOECastingTime > 0)
	{
		if (AOECastingTime - deltaTime < 0)
			AOECastingTime = 0;
		else
			AOECastingTime -= deltaTime;
	}
	if (AOECastingTime == 0)
		bIsCastingAOE = false;
}

void AArcanumCloneCharacter::OnCasting(ABaseCharacter * target)
{
	FVector location = GetActorLocation();
	FVector targetLocation = target->GetActorLocation();
	FRotator rotation = UKismetMathLibrary::FindLookAtRotation(location, targetLocation);

	GetCapsuleComponent()->SetWorldRotation(rotation);

	bIsCasting = true;

	CastingTime = 2;
}

void AArcanumCloneCharacter::OnCastingAOE()
{
	bIsCastingAOE = true;

	AOECastingTime = 3;
}

bool AArcanumCloneCharacter::CanLearnSpell(UMagic * magic)
{
	if (magic)
	{
		if (this->GetLevel() > magic->LevelRequirement && this->GetWillPower() > magic->WillPowerRequirement && this->GetSkillPoints() > 0)
		{
			this->SetSkillPoints(this->GetSkillPoints() - 1);
			return true;
		}
		else
			return false;
	}
	else
		return false;
}

void AArcanumCloneCharacter::ApplyDamage(ABaseCharacter * target, bool sourceIsPlayer)
{
	if (CanAttackAlready())
	{
		int damage = this->GetDamageValue();
		FString s = FString::FromInt(damage);
		FString answer = "";

		if (this->bAlive && target->CheckAlive())
		{
			if (!sourceIsPlayer)
			{
				if (bShallAttack)
				{
					if (target->GetCurrentHealth() - damage > 0)
					{
						target->AddToCurrentHealth(-damage);
						answer = (this->name + " hit " + target->GetName() + " for " + s + " damage");
					}
					else
					{
						answer = (target->GetName() + " ist gestorben :(");
						bInCombat = false;
						target->Death();
					}
				}
			}
			else
			{
				OnAttack(target);
				if (bShallAttack)
				{
					float damagePercentage = 0;
					if (target->GetCurrentHealth() - damage > 0)
					{
						target->AddToCurrentHealth(-damage);
						answer = (this->name + " hit " + target->GetName() + " for " + s + " damage");
						damagePercentage = (float)damage / target->GetMaxHealth();
					}
					else
					{
						damagePercentage = (float)target->GetCurrentHealth() / target->GetMaxHealth();
						answer = (target->GetName() + " ist gestorben :(");
						bInCombat = false;
						target->Death();
						xp += target->GetRewardEXP();
					}

					xp += (int)(damagePercentage*target->GetRewardEXP() * 2);
				}
			}
			CheckLevel();
			SetLogMessage(answer);
		}
	}
}

void AArcanumCloneCharacter::SaveGame()
{
	UMySaveGame* saveGame = dynamic_cast<UMySaveGame*>(UGameplayStatics::CreateSaveGameObject(UMySaveGame::StaticClass()));

	UWorld* world = GetWorld();

	saveGame->SavePlayers(world);
	saveGame->LevelName = world->GetName();

	UGameplayStatics::SaveGameToSlot(saveGame, "SaveGame1", 0);
}

void AArcanumCloneCharacter::SaveGameToSlot(FString slotName)
{
	UMySaveGame* saveGame = dynamic_cast<UMySaveGame*>(UGameplayStatics::CreateSaveGameObject(UMySaveGame::StaticClass()));

	UWorld* world = GetWorld();

	if (world)
	{
		saveGame->SavePlayers(world);
		saveGame->LevelName = world->GetName();
	}

	UGameplayStatics::SaveGameToSlot(saveGame, slotName, 0);
}

void AArcanumCloneCharacter::LoadLevelChange()
{
	UMySaveGame* saveGame = dynamic_cast<UMySaveGame*>(UGameplayStatics::CreateSaveGameObject(UMySaveGame::StaticClass()));

	UWorld* world = GetWorld();

	saveGame = dynamic_cast<UMySaveGame*>(UGameplayStatics::LoadGameFromSlot("LevelChangeSave", 0));

	if (saveGame)
	{
		FTransform defaultTransform = this->GetActorTransform(); // save default transform to keep default map position

		saveGame->LoadPlayerFromLevelChange(this, world);

		this->SetActorTransform(defaultTransform);	// load the default level transform
	}
}

FString AArcanumCloneCharacter::LoadGame()
{
	UMySaveGame* saveGame = dynamic_cast<UMySaveGame*>(UGameplayStatics::CreateSaveGameObject(UMySaveGame::StaticClass()));

	UWorld* world = GetWorld();

	saveGame = dynamic_cast<UMySaveGame*>(UGameplayStatics::LoadGameFromSlot("SaveGame1", 0));

	if (saveGame)
	{
		saveGame->LoadPlayers(world);
	}

	return saveGame->LevelName;
}

void AArcanumCloneCharacter::LoadGameFromSlot(FString slotName)
{
	UMySaveGame* saveGame = dynamic_cast<UMySaveGame*>(UGameplayStatics::CreateSaveGameObject(UMySaveGame::StaticClass()));

	UWorld* world = GetWorld();

	saveGame = dynamic_cast<UMySaveGame*>(UGameplayStatics::LoadGameFromSlot(slotName, 0));

	if (saveGame)
	{
		saveGame->LoadPlayers(world);
	}
}
