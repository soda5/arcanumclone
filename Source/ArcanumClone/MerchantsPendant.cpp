// Mischa Ahi / Philip Gandy @ 2016

#include "ArcanumClone.h"
#include "MerchantsPendant.h"

AMerchantsPendant::AMerchantsPendant()
{
	Name = TEXT("MerchantsPendant");

	bIsQuestRelevant = true;

	price = 150;

	ToolTip = TEXT("Merchants Pendant \n\nSomething seems strange with this Pendant. Everytime you look at it you are filled with joy. \nIt just seems to precious to wear it.");
}

void AMerchantsPendant::PickedUp()
{
	Super::PickedUp();
	AArcanumCloneCharacter* playerCharacter = (AArcanumCloneCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	playerCharacter->bFoundMerchantsPendant = true;
}

void AMerchantsPendant::Buy(AArcanumCloneCharacter * player)
{
	Super::Buy(player);
	player->bFoundMerchantsPendant = true;
}
