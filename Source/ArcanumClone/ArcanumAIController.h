// Mischa Ahi / Philip Gandy @ 2016

#pragma once

#include "AIController.h"
#include "ArcanumAIController.generated.h"

UCLASS()
class ARCANUMCLONE_API AArcanumAIController : public AAIController
{
	GENERATED_BODY()

	virtual void Possess(APawn* Pawn) override;

public:

	AArcanumAIController();

};