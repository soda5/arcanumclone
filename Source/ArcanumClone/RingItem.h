// Mischa Ahi / Philip Gandy @ 2016

#pragma once

#include "GearItem.h"
#include "RingItem.generated.h"

UCLASS()
class ARCANUMCLONE_API ARingItem : public AGearItem
{
	GENERATED_BODY()
	
public:
	ARingItem();

	void Use() override;

	void GiveStats() override;

	void RemoveStats() override;

protected:
	
	float damageModifier;

	float armorvalue;
	
};