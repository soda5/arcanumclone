// Mischa Ahi / Philip Gandy @ 2016

#pragma once

#include "GameFramework/Actor.h"
#include "Magic.generated.h"

class ABaseCharacter; //forward declaration to avoid double include


UCLASS(BlueprintType)
class ARCANUMCLONE_API UMagic : public UObject
{
	GENERATED_BODY()
	
public:	
	UMagic();

	UPROPERTY(BlueprintReadWrite)
		FString Name;

	UPROPERTY(EditAnywhere)
		int ManaCosts;

	UPROPERTY(EditAnywhere)
		int LevelRequirement;

	UPROPERTY(EditAnywhere)
		int WillPowerRequirement;

	UPROPERTY()
		bool bIsInstant; // true if the spell does not require a cast target
	
	UPROPERTY()
		bool bManaCostsHasBeenPayed;

	UPROPERTY(EditAnywhere)
		UTexture2D* MenuIcon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString Description;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		bool bIsLearned;

	UFUNCTION(BlueprintCallable, Category= "Magic")
		virtual void ActivateSpell(ABaseCharacter* target, UWorld* world);

	UFUNCTION()
		virtual void Costs(UWorld* world);

	UFUNCTION(BlueprintCallable, Category = "Magic")
		bool IsLearned();
	
};