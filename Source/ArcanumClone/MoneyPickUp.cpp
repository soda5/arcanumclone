// Mischa Ahi / Philip Gandy @ 2016

#include "ArcanumClone.h"
#include "MoneyPickUp.h"

AMoneyPickUp::AMoneyPickUp()
{
	bShallAppearInInventory = false;
}

void AMoneyPickUp::PickedUp()
{
	Super::PickedUp();
	
	AArcanumCloneCharacter* character = (AArcanumCloneCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	character->AddMoney(rewardMoney);
	SetActorEnableCollision(false);
	SetActorHiddenInGame(true);
}