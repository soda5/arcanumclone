// Mischa Ahi / Philip Gandy @ 2016

#pragma once

#include "TestPickUp.h"
#include "AmmunitionPickUp.generated.h"

UCLASS()
class ARCANUMCLONE_API AAmmunitionPickUp : public ATestPickUp
{
	GENERATED_BODY()
	
public:

	AAmmunitionPickUp();

	void PickedUp() override;

	UPROPERTY(EditAnywhere)
		int rewardAmmunition;

};