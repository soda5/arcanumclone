// Mischa Ahi / Philip Gandy @ 2016

#pragma once

#include "GameFramework/SaveGame.h"
#include "BaseCharacter.h"
#include "PlayerSaveValues.h"
#include "GearItem.h"
#include "PickUp.h"
#include "BootsItem.h"
#include "BreastPlateItem.h"
#include "HelmetItem.h"
#include "PantsItem.h"
#include "RingItem.h"
#include "ShieldItem.h"
#include "WeaponItem.h"
#include "AwesomeRingOfBlasphemy.h"
#include "HealthPotion.h"
#include "MerchantsPendant.h"
#include "MySaveGame.generated.h"

UCLASS()
class ARCANUMCLONE_API UMySaveGame : public USaveGame
{
	GENERATED_BODY()


public:

	UFUNCTION(BlueprintCallable, Category = "SaveGame")
		void SaveCharacterValues(ABaseCharacter* character);

	UFUNCTION(BlueprintCallable, Category = "LoadGame")
		void LoadCharacterValues(UWorld* const world, ABaseCharacter* character, int characterIndex);

	UFUNCTION()
		void SavePlayers(UWorld* const world);

	UFUNCTION()
		void LoadPlayers(UWorld* const world);

	UFUNCTION()
		void SavePlayerForLevelChange(ABaseCharacter* player);

	UFUNCTION()
		void LoadPlayerFromLevelChange(ABaseCharacter* player, UWorld* world);

	UPROPERTY()
		TArray<FPlayerValues> PlayerValues;

	UPROPERTY()
		FPlayerValues LevelChange;

	UPROPERTY(BlueprintReadOnly)
		FString LevelName;

private:

	void Deactivate(APickUp* pickUp);
};