
// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "ArcanumClone.h"
#include "ArcanumClonePlayerController.h"
#include "AI/Navigation/NavigationSystem.h"
#include "PickUp.h"

AArcanumClonePlayerController::AArcanumClonePlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::GrabHand;
	bEnableClickEvents = true;
	bEnableMouseOverEvents = true;
}

void AArcanumClonePlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	AArcanumCloneCharacter* character = dynamic_cast<AArcanumCloneCharacter*>(GetCharacter());

	character->MovementCooldown -= DeltaTime;

	// keep updating the destination every tick while desired
	if (bMoveToMouseCursor)
	{
		MoveToMouseCursor();
	}

	UWorld* world = GetWorld();
	AArcanumCloneCharacter* playerCharacter = dynamic_cast<AArcanumCloneCharacter*>(world->GetFirstPlayerController()->GetCharacter());

	if (playerCharacter->SelectedSpell)
	{
		if (playerCharacter->SelectedSpell->bIsInstant && !bUsedAOESpell)
		{
			if (playerCharacter->SelectedSpell->bIsLearned)
			{
				playerCharacter->OnCastingAOE();
			}
				bUsedAOESpell = true;
				CurrentMouseCursor = EMouseCursor::Crosshairs; // Reset Cursor
				playerCharacter->MovementCooldown = 0.5f; // so the character wont move immediately after action
		}
	}
	if (!playerCharacter->bIsCastingAOE && bUsedAOESpell)
	{
		if (playerCharacter->SelectedSpell->bIsLearned)
		{
			playerCharacter->SelectedSpell->ActivateSpell(playerCharacter, world);
		}
			bUsedAOESpell = false;
			playerCharacter->SelectedSpell = nullptr;
			playerCharacter->bHasSpellSelected = false;
	}

	if (!playerCharacter->bIsCasting && bUsedTargetSpell)
	{
		if (playerCharacter->SelectedSpell->bIsLearned)
		{
			playerCharacter->SelectedSpell->ActivateSpell(cachedTarget, world);
		}
			playerCharacter->SelectedSpell = nullptr;
			playerCharacter->bHasSpellSelected = false;
			bUsedTargetSpell = false;
	}
}

void AArcanumClonePlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	InputComponent->BindAction("SetDestination", IE_Pressed, this, &AArcanumClonePlayerController::OnSetDestinationPressed);
	InputComponent->BindAction("SetDestination", IE_Released, this, &AArcanumClonePlayerController::OnSetDestinationReleased);

}

void AArcanumClonePlayerController::MoveToMouseCursor()
{
	// Trace to see what is under the mouse cursor
	FHitResult Hit;
	GetHitResultUnderCursor(ECC_WorldDynamic, false, Hit);

	UWorld* world = GetWorld();

	if (world)
	{
		APlayerController* controller = world->GetFirstPlayerController();

		if (controller)
		{
			ACharacter* character = controller->GetCharacter();
			if (character)
			{
				AArcanumCloneCharacter* playerCharacter = dynamic_cast<AArcanumCloneCharacter*>(character);

				if (playerCharacter)
				{
					if (Hit.Actor == nullptr)
						return;
					if (playerCharacter->bHasSpellSelected)
					{
						if (Hit.Actor->GetClass()->IsChildOf(ABaseCharacter::StaticClass()))
						{
							ABaseCharacter* target = Cast<ABaseCharacter>(Hit.GetActor());
							cachedTarget = target;
							playerCharacter->OnCasting(target);
							bUsedTargetSpell = true;
							CurrentMouseCursor = EMouseCursor::GrabHand; // Reset Cursor
							playerCharacter->MovementCooldown = 0.5f; // so the character wont move immediately after action

						}
					}
					else if (Hit.Actor->GetClass()->IsChildOf(APickUp::StaticClass()))
					{
						FVector distance = (FVector)Hit.Actor->GetActorLocation() - (FVector)GetPawn()->GetActorLocation();

						APickUp* item = Cast<APickUp>(Hit.GetActor());

						if (distance.Size() < item->PickUpRange)
						{
							item->PickedUp();
							playerCharacter->MovementCooldown = 0.5f;// so the character wont move immediately after action
						}
					}
					else if (Hit.Actor->GetClass()->IsChildOf(ABaseCharacter::StaticClass()))
					{
						AArcanumCloneCharacter* playerCharacter = (AArcanumCloneCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);

						ABaseCharacter* otherCharacter = Cast<ABaseCharacter>(Hit.GetActor());

						if (playerCharacter->bInCombat && playerCharacter->GetDistanceTo(otherCharacter) <= playerCharacter->AttackRange)
						{
							playerCharacter->BeginCombat(otherCharacter);
							playerCharacter->MovementCooldown = 0.5f;// so the character wont move immediately after action
						}
					}
				}
				if (Hit.bBlockingHit)
				{
					if (playerCharacter->MovementCooldown <= 0)
						SetNewMoveDestination(Hit.ImpactPoint);
				}
			}
		}
	}
}

void AArcanumClonePlayerController::SetNewMoveDestination(const FVector DestLocation)
{
	APawn* const Pawn = GetPawn();
	if (Pawn)
	{
		UNavigationSystem* const NavSys = GetWorld()->GetNavigationSystem();
		float const Distance = FVector::Dist(DestLocation, Pawn->GetActorLocation());

		// We need to issue move command only if far enough in order for walk animation to play correctly
		if (NavSys && (Distance > 120.0f))
		{
			NavSys->SimpleMoveToLocation(this, DestLocation);
		}
	}
}

void AArcanumClonePlayerController::OnSetDestinationPressed()
{
	// set flag to keep updating destination until released
	bMoveToMouseCursor = true;
}

void AArcanumClonePlayerController::OnSetDestinationReleased()
{
	// clear flag to indicate we should stop updating the destination
	bMoveToMouseCursor = false;
}