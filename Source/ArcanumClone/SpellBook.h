// Mischa Ahi / Philip Gandy @ 2016

#pragma once

#include "GameFramework/Actor.h"
#include "Magic.h"
#include "Magic_Air_1.h"
#include "Magic_Air_2.h"
#include "Magic_Air_3.h"
#include "Magic_Air_4.h"
#include "Magic_Earth_1.h"
#include "Magic_Earth_2.h"
#include "Magic_Earth_3.h"
#include "Magic_Earth_4.h"
#include "Magic_Water_1.h"
#include "Magic_Water_2.h"
#include "Magic_Water_3.h"
#include "Magic_Water_4.h"
#include "Magic_Fire_1.h"
#include "Magic_Fire_2.h"
#include "Magic_Fire_3.h"
#include "Magic_Fire_4.h"
#include "SpellBook.generated.h"

UCLASS(BlueprintType)
class ARCANUMCLONE_API USpellBook : public UObject
{
	GENERATED_BODY()

public:
	USpellBook();

	UPROPERTY(BlueprintReadWrite)
		TArray<UMagic*> Spells;

	UFUNCTION()
		void InitSpells();

private:

	UFUNCTION()
		void InitSpellAir1();

	UFUNCTION()
		void InitSpellAir2();	
	
	UFUNCTION()
		void InitSpellAir3();

	UFUNCTION()
		void InitSpellAir4();

	UFUNCTION()
		void InitSpellEarth1();

	UFUNCTION()
		void InitSpellEarth2();

	UFUNCTION()
		void InitSpellEarth3();

	UFUNCTION()
		void InitSpellEarth4();

	UFUNCTION()
		void InitSpellWater1();

	UFUNCTION()
		void InitSpellWater2();

	UFUNCTION()
		void InitSpellWater3();	
	
	UFUNCTION()
		void InitSpellWater4();

	UFUNCTION()
		void InitSpellFire1();	
	
	UFUNCTION()
		void InitSpellFire2();

	UFUNCTION()
		void InitSpellFire3();

	UFUNCTION()
		void InitSpellFire4();

};
