// Mischa Ahi @ 2016

#include "ArcanumClone.h"
#include "TestPickUp.h"

ATestPickUp::ATestPickUp()
{
	LogText = "You got 1 additional Skill Point.";
}

void ATestPickUp::PickedUp()
{
	AArcanumCloneCharacter* character = (AArcanumCloneCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);

	character->SetLogMessage(LogText);

	Super::PickedUp();
	
	character->GiveXP(character->ReturnExpToNextLevel());
	character->CheckLevel();
}