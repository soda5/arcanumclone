// Mischa Ahi / Philip Gandy @ 2016

#pragma once

#include "Magic.h"
#include "EngineUtils.h"
#include "Magic_Air_3.generated.h"

UCLASS()
class ARCANUMCLONE_API UMagic_Air_3 : public UMagic
{
	GENERATED_BODY()

public:
	UMagic_Air_3();

	UFUNCTION()
		virtual void Costs(UWorld* world) override;

	UFUNCTION()
		virtual void ActivateSpell(ABaseCharacter* target, UWorld* world) override;

	UPROPERTY()
		TArray<ABaseCharacter*> NearCharacter;

	UFUNCTION()
		void PushBack(ABaseCharacter* player, ABaseCharacter* target);

	UPROPERTY(EditAnywhere)
		int PushRange;
};
