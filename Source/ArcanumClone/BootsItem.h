// Mischa Ahi / Philip Gandy @ 2016

#pragma once

#include "GearItem.h"
#include "BootsItem.generated.h"

UCLASS()
class ARCANUMCLONE_API ABootsItem : public AGearItem
{
	GENERATED_BODY()
	
public:
	ABootsItem();

	void Use() override;

	void GiveStats() override;

	void RemoveStats() override;

protected:
	
	int armorvalue;

};