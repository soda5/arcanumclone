// Mischa Ahi / Philip Gandy @ 2016

#include "ArcanumClone.h"
#include "Magic_Earth_1.h"
#include "BaseCharacter.h"

UMagic_Earth_1::UMagic_Earth_1()
{
	ManaCosts = 5;
}

void UMagic_Earth_1::Costs(UWorld* world)
{
	Super::Costs(world);

	if (world)
	{
		APlayerController* controller = world->GetFirstPlayerController();

		if (controller)
		{
			ACharacter* character = controller->GetCharacter();
			if (character)
			{
				ABaseCharacter* playerCharacter = dynamic_cast<ABaseCharacter*>(character);
				if (playerCharacter->GetCurrentMana() >= ManaCosts)
				{
					playerCharacter->SetCurrentMana(-ManaCosts);
					bManaCostsHasBeenPayed = true;
				}
				else
					bManaCostsHasBeenPayed = false;
			}
		}
	}
}

void UMagic_Earth_1::ActivateSpell(ABaseCharacter* target, UWorld* world)
{
	Super::ActivateSpell(target, world);

	if (world)
	{
		APlayerController* controller = world->GetFirstPlayerController();

		if (controller)
		{
			ACharacter* character = controller->GetCharacter();
			if (character)
			{
				ABaseCharacter* playerCharacter = dynamic_cast<ABaseCharacter*>(character);

				if (bManaCostsHasBeenPayed)
				{
					if (target)
					{
						target->IncreaseStaminaByMagic();
						target->IncreaseStaminaByMagic();
						target->IncreaseStaminaByMagic();
						target->IncreaseStaminaByMagic();
					}
				}
			}
		}
	}
}