// Mischa Ahi / Philip Gandy @ 2016

#include "ArcanumClone.h"
#include "Retep.h"
#include "MoneyPickUp.h"
#include "AwesomeRingOfBlasphemy.h"
#include "WeaponItem.h"

ARetep::ARetep()
{
	healthGrowth = 12;
	rewardExp = 250;
}

void ARetep::SpawnLoot()
{
	if (UWorld* const World = GetWorld())
	{
		float timer = 0;
		for (size_t i = 0; i < 25; i++)
		{
			if (i == 10)
			{
				if (EpicLoot)
				{
					AWeaponItem* LootItem = World->SpawnActor<AWeaponItem>(EpicLoot, GetActorLocation(), FRotator(0, 0, 0));
				}
			}
			else
				if (MoneyBP)
				{
					AMoneyPickUp* LootItem = World->SpawnActor<AMoneyPickUp>(MoneyBP, GetActorLocation(), FRotator(0, 0, 0));
					LootItem->rewardMoney = FMath::RandRange(25, 75);
				}
			while (timer <= 2500)
			{
				timer += deltaTime;
			}
			timer = 0;
		}
	}
}

void ARetep::Death()
{
	SetActorEnableCollision(false);
	SetActorTickEnabled(false);
	SetActorHiddenInGame(true);

	currentHealth = 0;
	bAlive = false;
	bInCombat = false;

	SpawnLoot();
}