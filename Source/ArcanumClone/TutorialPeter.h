// Mischa Ahi / Philip Gandy @ 2016

#pragma once

#include "BaseNPC.h"
#include "TutorialPeter.generated.h"

UCLASS()
class ARCANUMCLONE_API ATutorialPeter : public ABaseNPC
{
	GENERATED_BODY()

public:

	ATutorialPeter();

		virtual void SetText() override;
	
		virtual int ReturnNextDialogPosition(int choosenAnswer) override;

protected:

	int silenceCounter;
};