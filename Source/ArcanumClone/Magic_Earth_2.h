// Mischa Ahi / Philip Gandy @ 2016

#pragma once

#include "Magic.h"
#include "Magic_Earth_2.generated.h"

UCLASS()
class ARCANUMCLONE_API UMagic_Earth_2 : public UMagic
{
	GENERATED_BODY()
	
public:

	UMagic_Earth_2();

	UFUNCTION()
		virtual void Costs(UWorld* world) override;

	UFUNCTION()
		virtual void ActivateSpell(ABaseCharacter* target, UWorld* world) override;
	
};
