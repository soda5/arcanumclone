// Mischa Ahi @ 2016

#pragma once

#include "GameFramework/Actor.h"
#include "ArcanumCloneCharacter.h"
#include "PickUp.generated.h"

UCLASS()
class ARCANUMCLONE_API APickUp : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	APickUp();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void PickedUp();

	UFUNCTION(BlueprintPure, Category = "Inventory")
		FString GetToolTip();

	UFUNCTION(BlueprintPure, Category = "Inventory")
		UTexture2D* GetTexture();

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* Mesh;

	UPROPERTY(EditAnywhere)
		FString ToolTip;

	UPROPERTY(EditAnywhere)
		UTexture2D* Icon;

	//Range to pick up items
	UPROPERTY(EditAnywhere)
		int PickUpRange;

	UPROPERTY(EditAnywhere)
		FString LogText;

	UFUNCTION(BlueprintCallable, Category = "Items")
		virtual void Use();

	UFUNCTION(BlueprintCallable, Category = "Items")
		void Sell(AArcanumCloneCharacter* player, ABaseCharacter* merchant);

	UFUNCTION(BlueprintCallable, Category = "Items")
		virtual void Buy(AArcanumCloneCharacter* player);

	UFUNCTION()
		bool IsAttackable();

	int InventorySpace;

	int currentHealth;

	UPROPERTY(EditAnywhere)
		int maxHealth;

	bool bPickedUp;

	UPROPERTY(EditAnywhere)
		int price;

	UPROPERTY()
		FString Name; // for SaveGame purpose

	UPROPERTY()
		bool bIsQuestRelevant;

protected:

	UPROPERTY(EditAnywhere)
		USoundBase* pickUpSound;

	bool bAttackable;

	bool bShallAppearInInventory;

};