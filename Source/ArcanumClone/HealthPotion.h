// Mischa Ahi / Philip Gandy @ 2016

#pragma once

#include "PickUp.h"
#include "HealthPotion.generated.h"

/**
 * 
 */
UCLASS()
class ARCANUMCLONE_API AHealthPotion : public APickUp
{
	GENERATED_BODY()
	
public:

	AHealthPotion();

	virtual void Use() override;

private:

	int healamount;
	
	
};
