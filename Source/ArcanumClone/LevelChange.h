// Mischa Ahi / Philip Gandy @ 2016

#pragma once

#include "GameFramework/Actor.h"
#include "ArcanumCloneCharacter.h"
#include "MySaveGame.h"
#include "SwitchLevelValues.h"
#include "LevelChange.generated.h"

UCLASS()
class ARCANUMCLONE_API ALevelChange : public AActor
{
	GENERATED_BODY()
	
public:	
	ALevelChange();

	virtual void BeginPlay() override;
	
	virtual void Tick( float DeltaSeconds ) override;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* Mesh;

	UPROPERTY(EditAnywhere)
		int CanUseRange;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LevelChange")
		FString TargetMap;

	UFUNCTION(BlueprintCallable, Category = "LevelChange")
		void Activate(FName levelName, AArcanumCloneCharacter* player);
};