// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "ArcanumClone.h"


IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ArcanumClone, "ArcanumClone" );

DEFINE_LOG_CATEGORY(LogArcanumClone)