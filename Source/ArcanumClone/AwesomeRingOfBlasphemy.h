// Mischa Ahi / Philip Gandy @ 2016

#pragma once

#include "RingItem.h"
#include "AwesomeRingOfBlasphemy.generated.h"

UCLASS()
class ARCANUMCLONE_API AAwesomeRingOfBlasphemy : public ARingItem
{
	GENERATED_BODY()

public:

	AAwesomeRingOfBlasphemy();

	virtual void GiveStats() override;

	virtual void RemoveStats() override;

private:

	int mana;

	int healthPoints;

};