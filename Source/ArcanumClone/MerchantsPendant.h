// Mischa Ahi / Philip Gandy @ 2016

#pragma once

#include "PickUp.h"
#include "MerchantsPendant.generated.h"

/**
 * 
 */
UCLASS()
class ARCANUMCLONE_API AMerchantsPendant : public APickUp
{
	GENERATED_BODY()

public:

		AMerchantsPendant();

		virtual void PickedUp() override;

		virtual void Buy(AArcanumCloneCharacter * player) override;
	
};
