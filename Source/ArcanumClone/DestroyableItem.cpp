// Mischa Ahi @ 2016

#include "ArcanumClone.h"
#include "DestroyableItem.h"


ADestroyableItem::ADestroyableItem()
{
	bAttackable = true;
	LogText = "Destroyable Item destroyed.";
}

void ADestroyableItem::BeginPlay()
{
	Super::BeginPlay();

	currentHealth = maxHealth;
}

void ADestroyableItem::PickedUp()
{
	Super::PickedUp();

	AArcanumCloneCharacter* character = (AArcanumCloneCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);

	int damage = character->GetDamageValue();

	character->ApplyDamageToItem(this, true);
	UE_LOG(LogTemp, Warning, TEXT("health: %d "), currentHealth);
}