// Mischa Ahi @ 2016

#pragma once

#include "GameFramework/Character.h"
#include "ArcanumAIController.h"
#include "Magic.h"
#include "SpellBook.h"
#include "BaseCharacter.generated.h"

class APickUp;
class AGearItem;


/**
* All Character values and health and mana for UI logic
* @param Strength - Increases your HP by 2 and your carryWeight by 500 per point.
* @param Stamina - Increases your Mana by 2 per point.
* @param Intelligence - Every 4. point increases your SpellLimit.
* @param Willpower - Increases your HP and Mana by 1 point each.
* @param Dexterity - Increases your movement speed and damage for some weapons.
* @param Perception - Increases your accuracy.
* @param Beauty - Influences the reaction from NPCs.
* @param Charisma - Increases your chance of success for persuation.
* @param Health - Health is important to stay alive.
* @param Mana - Mana is needed to Cast Magic.
*/
UENUM(BluePrintType)
enum class ELastGivenSkill : uint8
{
	Strenght, Stamina, Intelligence, Willpower, Dexterity, Perception, Beauty, Charisma, Health, Mana
};

UCLASS(abstract)
class ARCANUMCLONE_API ABaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ABaseCharacter();

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

	// Called to bind functionality to input (just for inherit player)
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite)
		UStaticMeshComponent* Helmet;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite)
		UStaticMeshComponent* Weapon;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item")
		TSubclassOf<APickUp> AmmoBP;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item")
		TSubclassOf<APickUp> MoneyBP;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item")
		TSubclassOf<APickUp> BootsBP;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item")
		TSubclassOf<APickUp> BreastPlateBP;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item")
		TSubclassOf<APickUp> PantsBP;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item")
		TSubclassOf<APickUp> WeaponBP;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item")
		TSubclassOf<APickUp> HelmetBP;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item")
		TSubclassOf<APickUp> RingBP;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item")
		TSubclassOf<APickUp> ShieldBP;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item")
		TSubclassOf<APickUp> HPPotionBP;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item")
		TSubclassOf<APickUp> MerchantsPendantBP;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item")
		TSubclassOf<APickUp> AwesomeRingOfBlasphemyBP;

	UPROPERTY(EditAnywhere)
		int NPCNumber;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		/**
		* Mesh for the Magic tranformation of the 4. Air Spell
		* @param Mesh_Air - Mesh of the 4. air spell transformation
		*/
		USkeletalMesh* Mesh_Air;


	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		/**
		* Mesh for the Magic tranformation of the 4. Earth Spell
		* @param Mesh_Earth - Mesh of the 4. air spell transformation
		*/
		USkeletalMesh* Mesh_Earth;


	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		/**
		* Mesh for the Magic tranformation of the 4. Water Spell
		* @param Mesh_Water - Mesh of the 4. air spell transformation
		*/
		USkeletalMesh* Mesh_Water;


	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		/**
		* Mesh for the Magic tranformation of the 4. Fire Spell
		* @param Mesh_Fire - Mesh of the 4. air spell transformation
		*/
		USkeletalMesh* Mesh_Fire;

	// 

	UPROPERTY(EditAnywhere)
		int UndoSkillPoints;

	UPROPERTY(EditAnywhere)
		TArray<ELastGivenSkill> AllChoosenSkills;

	UPROPERTY(BlueprintReadOnly)
		bool bIsCasting = false;

	UPROPERTY(BlueprintReadOnly)
		bool bIsCastingAOE = false;

	//Animation/////////////////////////////

	UPROPERTY(BlueprintReadOnly)
		bool bHasAttackStarted;

	UPROPERTY(BlueprintReadWrite)
		float AttackAnimationCooldown;

	UFUNCTION()
		void OnAttack(ABaseCharacter* target);

	///////////////////////////////////////
	UFUNCTION()
		virtual void BeginCombat(ABaseCharacter* combatTarget);

	UFUNCTION()
		virtual int GetDamageValue();

	UFUNCTION()
		bool CanAttackAlready();

	UFUNCTION()
		virtual void ApplyDamage(ABaseCharacter* target, bool sourceIsPlayer);

	UFUNCTION()
		void ApplyDamageToItem(APickUp* target, bool sourceIsPlayer);

	UFUNCTION()
		/**
		* This function will deactivate the Player
		*/
		virtual void Death();

	UFUNCTION(BlueprintPure, Category = "Log")
		FString GetLog();

	UFUNCTION()
		void FollowCharacter(ABaseCharacter* character);

	UFUNCTION(BlueprintCallable, Category = "LogMessage")
		void SetLogMessage(FString text);

	UFUNCTION(BlueprintPure, Category = "Icon")
		UTexture2D* GetIcon();

	UFUNCTION(BlueprintCallable, Category = "Icon")
		void SetIcon(UTexture2D* icon);

	UFUNCTION(BlueprintPure, Category = "CheckAlive")
		bool CheckAlive();

	UFUNCTION(BlueprintCallable, Category = "EXPSystem")
		int GetRewardEXP();

	UFUNCTION(BlueprintCallable, Category = "EXPSystem")
		void SetRewardEXP(int value);

	UFUNCTION(BlueprintPure, Category = "Name")
		FString GetName();

	UFUNCTION(BlueprintCallable, Category = "Name")
		void SetName(FString name);

	UFUNCTION(BlueprintPure, Category = "Race")
		FString GetRace();

	UFUNCTION(BlueprintCallable, Category = "Race")
		void SetRace(FString race);

	UFUNCTION(BlueprintPure, Category = "Sex")
		FString GetSex();

	UFUNCTION(BlueprintCallable, Category = "Sex")
		void SetSex(FString sex);

	UFUNCTION(BlueprintPure, Category = "Level")
		int GetLevel();

	UFUNCTION(BlueprintPure, Category = "SkillPoints")
		int GetSkillPoints();

	UFUNCTION(BlueprintCallable, Category = "SkillPoints")
		void SetSkillPoints(int points);

	UFUNCTION(BlueprintCallable, Category = "SkillPoints")
		void IncreaseSkillPoints();

	UFUNCTION(BlueprintPure, Category = "XP")
		int GetXp();

	UFUNCTION(BlueprintCallable, Category = "XP")
		void SetXp(int xp);

	UFUNCTION(BlueprintPure, Category = "Health")
		int GetMaxHealth();

	UFUNCTION(BlueprintCallable, Category = "Health")
		void SetMaxHealth(int health);

	UFUNCTION(BlueprintPure, Category = "Health")
		int GetCurrentHealth();

	UFUNCTION(BlueprintCallable, Category = "Health")
		void SetCurrentHealth(int healthChange);

	UFUNCTION(BlueprintPure, Category = "Health")
		int GetHealthGrowth();

	UFUNCTION(BlueprintCallable, Category = "Health")
		void SetHealthGrowth(int growth);

	UFUNCTION(BlueprintCallable, Category = "Health")
		void AddToCurrentHealth(int healthChange);

	UFUNCTION(BlueprintCallable, Category = "Cheats")
		void CheatHealth();

	UFUNCTION(BlueprintPure, Category = "Mana")
		int GetMaxMana();

	UFUNCTION(BlueprintCallable, Category = "Mana")
		void SetMaxMana(int mana);

	UFUNCTION(BlueprintPure, Category = "Mana")
		int GetCurrentMana();

	UFUNCTION(BlueprintPure, Category = "Mana")
		int GetManaGrowth();

	UFUNCTION(BlueprintCallable, Category = "Mana")
		void SetManaGrowth(int growth);

	UFUNCTION()
		void SetCurrentMana(int change);

	UFUNCTION(BlueprintCallable, Category = "Cheats")
		void CheatMana();

	UFUNCTION(BlueprintPure, Category = "CarryWeight")
		int GetCarryWeight();

	UFUNCTION(BlueprintCallable, Category = "CarryWeight")
		void SetCarryWeight(int value);

	UFUNCTION(BlueprintPure, Category = "Movement")
		int GetMaxMovementSpeed();

	UFUNCTION()
		int GetMovementSpeed();

	UFUNCTION(BlueprintCallable, Category = "MovemenSpeed")
		void SetMovementSpeed(int speed);

	UFUNCTION(BlueprintPure, Category = "SpellLimit")
		int CalculateSpellLimit();

	UFUNCTION(BlueprintPure, Category = "SpellLimit")
		int GetSpellLimit();

	UFUNCTION(BlueprintCallable, Category = "SpellLimit")
		void SetSpellLimit(int limit);

	UFUNCTION(BlueprintCallable, Category = "Strength")
		void IncreaseStrength();

	UFUNCTION(BlueprintCallable, Category = "Mana")
		void IncreaseManaGrowth(int amount);

	UFUNCTION(BlueprintCallable, Category = "Health")
		void IncreaseHealthGrowth(int amount);

	UFUNCTION()
		void IncreaseStrengthByMagic();

	UFUNCTION(BlueprintCallable, Category = "Strength")
		void DecreaseStrength();

	UFUNCTION(BlueprintPure, Category = "Strength")
		int GetStrength();

	UFUNCTION(BlueprintCallable, Category = "Strength")
		void SetStrength(int strength);

	UFUNCTION(BlueprintCallable, Category = "Stamina")
		void IncreaseStamina();

	UFUNCTION()
		void IncreaseStaminaByMagic();

	UFUNCTION(BlueprintCallable, Category = "Stamina")
		void DecreaseStamina();

	UFUNCTION(BlueprintPure, Category = "Stamina")
		int GetStamina();

	UFUNCTION(BlueprintCallable, Category = "Stamina")
		void SetStamina(int stamina);

	UFUNCTION(BlueprintCallable, Category = "Intelligence")
		void IncreaseIntelligence();

	UFUNCTION()
		void IncreaseIntelligenceByMagic();

	UFUNCTION(BlueprintCallable, Category = "Intelligence")
		void DecreaseIntelligence();

	UFUNCTION(BlueprintCallable, Category = "Intelligence")
		void SetIntelligence(int intelligence);

	UFUNCTION(BlueprintPure, Category = "Intelligence")
		int GetIntelligence();

	UFUNCTION(BlueprintCallable, Category = "WillPower")
		void IncreaseWillPower();

	UFUNCTION(BlueprintCallable, Category = "WillPower")
		void DecreaseWillPower();

	UFUNCTION(BlueprintPure, Category = "WillPower")
		int GetWillPower();

	UFUNCTION(BlueprintCallable, Category = "WillPower")
		void SetWillPower(int will);

	UFUNCTION(BlueprintCallable, Category = "Dexterity")
		void IncreaseDexterity();

	UFUNCTION(BlueprintCallable, Category = "Dexterity")
		void DecreaseDexterity();

	UFUNCTION(BlueprintPure, Category = "Dexterity")
		int GetDexterity();

	UFUNCTION(BlueprintCallable, Category = "Dexterity")
		void SetDexterity(int dex);

	UFUNCTION(BlueprintCallable, Category = "Perception")
		void IncreasePerception();

	UFUNCTION(BlueprintCallable, Category = "Perception")
		void DecreasePerception();

	UFUNCTION(BlueprintPure, Category = "Perception")
		int GetPerception();

	UFUNCTION(BlueprintCallable, Category = "Perception")
		void SetPerception(int perception);

	UFUNCTION(BlueprintCallable, Category = "Beauty")
		void IncreaseBeauty();

	UFUNCTION()
		void IncreaseBeautyByMagic();

	UFUNCTION(BlueprintCallable, Category = "Beauty")
		void DecreaseBeauty();

	UFUNCTION(BlueprintPure, Category = "Beauty")
		int GetBeauty();

	UFUNCTION(BlueprintCallable, Category = "Beauty")
		void SetBeauty(int beauty);

	UFUNCTION(BlueprintCallable, Category = "Charisma")
		void IncreaseCharisma();

	UFUNCTION(BlueprintCallable, Category = "Charisma")
		void DecreaseCharisma();

	UFUNCTION(BlueprintPure, Category = "Charisma")
		int GetCharisma();

	UFUNCTION(BlueprintCallable, Category = "Charisma")
		void SetCharisma(int charisma);

	UFUNCTION(BlueprintCallable, Category = "Health")
		void IncreaseHealth();

	UFUNCTION(BlueprintCallable, Category = "Health")
		void DecreaseHealth();

	UFUNCTION(BlueprintCallable, Category = "Mana")
		void IncreaseMana();

	UFUNCTION(BlueprintCallable, Category = "Mana")
		void DecreaseMana();

	UFUNCTION(BlueprintPure, Category = "Karma")
		int GetKarma();

	UFUNCTION(BlueprintCallable, Category = "Karma")
		void SetKarma(int karma);

	UFUNCTION(BlueprintPure, Category = "Alignment")
		int GetAlignment();

	UFUNCTION(BlueprintCallable, Category = "Alignment")
		void SetAlignment(int value);

	UFUNCTION(BlueprintCallable, Category = "Armor")
		int GetArmor();

	UFUNCTION(BlueprintCallable, Category = "Armor")
		void SetArmor(int armor);

	UFUNCTION()
		void AddArmor(int value);

	UFUNCTION()
		int GetDodge();

	UFUNCTION(BlueprintCallable, Category = "Dodge")
		void SetDodge(int dodge);

	UFUNCTION()
		int GetMeele();

	UFUNCTION(BlueprintCallable, Category = "Meele")
		void SetMeele(int meele);

	UFUNCTION()
		int GetBow();

	UFUNCTION(BlueprintCallable, Category = "Bow")
		void SetBow(int bow);

	UFUNCTION(BlueprintPure, Category = "Prowling")
		int GetProwling();

	UFUNCTION(BlueprintCallable, Category = "Prowling")
		void SetProwling(int value);

	UFUNCTION(BlueprintPure, Category = "PickPocket")
		int GetPickPocket();

	UFUNCTION(BlueprintCallable, Category = "PickPocket")
		void SetPickPocket(int pick);

	UFUNCTION(BlueprintPure, Category = "Haggle")
		int GetHaggle();

	UFUNCTION(BlueprintCallable, Category = "Haggle")
		void SetHaggle(int haggle);

	UFUNCTION()
		int GetPersuation();

	UFUNCTION(BlueprintCallable, Category = "Persuation")
		void SetPersuation(int value);

	UFUNCTION()
		int GetPickLocks();

	UFUNCTION(BlueprintCallable, Category = "PickLocks")
		void SetPickLocks(int value);

	UFUNCTION(BlueprintCallable, Category = "Skillpoints")
		void ResetUndoSkillPoints();

	UFUNCTION(BlueprintCallable, Category = "EXPSystem")
		void GiveXP(int amount);

	UFUNCTION(BlueprintPure, Category = "EXPSystem")
		int ReturnExpToNextLevel();

	UFUNCTION()
		void CheckLevel();

	UFUNCTION()
		void LevelUp();

	// Combat

	UFUNCTION(BlueprintPure, Category = "AttackCooldown")
		float GetAttackCooldown();

	UFUNCTION(BlueprintCallable, Category = "AttackCooldown")
		void SetAttackCooldown(float value);

	UPROPERTY(BlueprintReadOnly)
		bool bInCombat;

	UFUNCTION(BlueprintCallable, Category = "Combat")
		void SetShallAttack(bool shallAttack);

	UFUNCTION(BlueprintCallable, Category = "Combat")
		bool GetShallAttack();

	// Combat end

	// Magic

	UPROPERTY(BlueprintReadWrite)
		bool bHasSpellSelected;

	UPROPERTY(BlueprintReadWrite)
		UMagic* SelectedSpell;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		USpellBook* SpellBook;

	// Magic end

	// Inventory

	UPROPERTY(EditAnywhere)
		TArray<AGearItem*> Gear;

	UFUNCTION(BlueprintCallable, Category = "Equipment")
		void EquipItem(AGearItem* Item);

	UFUNCTION(BlueprintCallable, Category = "Equipment")
		void UnequipItem(int slot);

	UFUNCTION(BlueprintPure, Category = "Inventory")
		int GetMoney();

	UFUNCTION(BlueprintCallable, Category = "Inventory")
		void SetMoney(int money);

	UFUNCTION(BlueprintCallable, Category = "Inventory")
		void AddMoney(int addedMoney);

	UFUNCTION(BlueprintPure, Category = "Inventory")
		int GetAmmunition();

	UFUNCTION(BlueprintCallable, Category = "Inventory")
		void SetAmmunition(int ammo);

	UFUNCTION(BlueprintCallable, Category = "Inventory")
		void AddAmmunition(int addedAmmunition);

	UFUNCTION(BlueprintCallable, Category = "Inventory")
		void PickUpItem(APickUp* item);

	UFUNCTION(BlueprintCallable, Category = "Inventory")
		void EmptyInventory();

	UFUNCTION()
		bool StillSpaceInInventory(int neededSpace);

	UFUNCTION(BlueprintPure, Category = "Inventory")
		TArray<APickUp*> GetInventory();

	UFUNCTION(BlueprintCallable, Category = "Inventory")
		void SetInventory(TArray<APickUp*> inventory);

	UFUNCTION(BlueprintCallable, Category = "Inventory")
		void AddToInventory(APickUp* item);

	UFUNCTION(BlueprintCallable, Category = "Inventory")
		void RemoveFromInventory(APickUp* item);

	UFUNCTION(BlueprintPure, Category = "Inventory")
		int GetInventorySpace();

	UFUNCTION(BlueprintCallable, Category = "Inventory")
		void SetInventorySpace(int space);

	UFUNCTION(BlueprintPure, Category = "Equipment")
		TArray<AGearItem*> GetGear();

	// Movement

	UPROPERTY(EditAnywhere)
		bool bShallFollow;

	UPROPERTY(EditAnywhere)
		bool bShallAttack;

	UPROPERTY(EditAnywhere)
		int PerceptionRange;

	UPROPERTY(EditAnywhere)
		int AttackRange;

	UPROPERTY()
		float MovementCooldown;

	UPROPERTY()
		int BonusMovementSpeed;

	UPROPERTY()
		int MovementSpeedSlowValue;

	UPROPERTY()
		float MovementSlowDuration;


	// Movement end

	// State Machine

	UFUNCTION()
		void Idle();

	UFUNCTION()
		void Seek();

	UFUNCTION()
		void Attack();

	UFUNCTION()
		void Dead();

	// State Machine end

	bool bPlayer;

	bool bAlive;

	UPROPERTY()
		float WeaponAttackspeed;

	UPROPERTY()
		float AttackSpeedDebuff;

	UPROPERTY()
		float AttackSpeedDebuffDuration;

	UPROPERTY()
		float DamageModifier = 1;

//DialogBools and stuff

	UPROPERTY()
		bool bStartedAttackCaveGoblins;

	UPROPERTY()
		bool bStartedMerchantsPendant;

	UPROPERTY(BlueprintReadWrite)
		bool bFinishedAttackCaveGoblinsObjective;

	UPROPERTY()
		bool bFoundMerchantsPendant;

	UPROPERTY()
		bool bTalkedWithThePriest;

protected:

	FString currentLogMessage;

	float deltaTime;

	// 0 = Idle | 1 = Seek | 2 = Attack
	int State;

	// Overview

	int rewardExp;

	UPROPERTY(EditAnywhere)
		UTexture2D* icon;

	UPROPERTY(EditAnywhere)
		FString name;

	UPROPERTY(EditAnywhere)
		FString race;

	UPROPERTY(EditAnywhere)
		FString sex;

	UPROPERTY()
		int level;


	/**
	* The Value of this Character how much free skill points he has currently
	* @param skillPoints - Free Skill points the player can spend. Should not be higher than 64.
	*/
	int skillPoints;

	/**
	* The Value of this Character how much XP he has currently
	* @param xp - current experience Value, needed for leveling
	*/
	int xp;

	// Typical values

	/**
	* The Value of this Character how much additional Health he has skilled currently
	* @param healthGrowth - How many Skillpoints you put into Health directly
	*/
	int healthGrowth;

	/**
	* The Value of this Character how much additional Mana he has skilled currently
	* @param manaGrowth - How many Skillpoints you put into Mana directly
	*/
	int manaGrowth;

	/**
	* This is the characters money
	* @param money - How rich the character is
	*/
	int money;

	/**
	* This is the characters ammo for firearms
	* @param ammunition - Amount of Bullets
	*/
	int ammunition;

	/**
	* This is the characters Strength value
	* @param strength - Increases Health by 2 and carryWeight by 500 per point
	*/
	int strength;

	/**
	* This is the characters Intelligence value
	* @param intelligence - Increases spell limit per every 4 points
	*/
	int intelligence;

	/**
	* This is the characters Stamina value
	* @param stamina - Increases Mana by 2 per point
	*/
	int stamina;

	/**
	* This is the characters Willpower value
	* @param willPower - Increases Health and Mana by 1 per point
	*/
	int willPower;

	/**
	* This is the characters Dexterity value
	* @param dexterity - Increases movement speed and attack speed
	*/
	int dexterity;

	/**
	* This is the characters Perception value
	* @param perception - Increases ShootRange and accuracy
	*/
	int perception;

	/**
	* This is the characters Beauty value
	* @param beauty - Improves NPC's reaction to you
	*/
	int beauty;

	/**
	* This is the characters Charisma value
	* @param charisma - Helps your persuating other Characters
	*/
	int charisma;

	/**
	* This is the characters maximal health
	* @param maxHealth - This is the amount of Health without any damage taken
	*/
	int maxHealth;

	/**
	* This is the characters current health
	* @param currentHealth - This is the amount of Health the character currently has
	*/
	int currentHealth;

	/**
	* This is the characters maximal mana
	* @param maxMana - This is the amount of mana without any mana spend
	*/
	int maxMana;

	/**
	* This is the characters current mana
	* @param maxMana - This is the amount of mana the character currently has
	*/
	int currentMana;

	int armor;

	// Skills

	int bow;

	int dodge;

	int meele;

	int pickPocket;

	int prowling;

	int gambling;

	int haggle; // feilschen

	int persuation; // überzeugen

	int pickLocks; // schlösserknacken

	// General values

	int carryWeight;

	int movementSpeed;

	int spellLimit;

	int karma;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TArray<APickUp*> inventory;

	int inventorySpace;

	int alignment; // ausrichtung technologie oder magie

	// Movement

	// This function is exclusive for NPC's
	UFUNCTION()
		void MoveTo(const FVector destination);

	// Movement end

	// Combat

	UPROPERTY()
		float timeSinceLastAttack;

	UPROPERTY()
		float attackCooldown;


	// this is true per default, set it to false if no permanent combat
	UPROPERTY()
		bool bStateMachineActive;

	// Combat end

};
