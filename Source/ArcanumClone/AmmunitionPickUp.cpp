// Mischa Ahi / Philip Gandy @ 2016

#include "ArcanumClone.h"
#include "AmmunitionPickUp.h"

AAmmunitionPickUp::AAmmunitionPickUp()
{
	bShallAppearInInventory = false;
}

void AAmmunitionPickUp::PickedUp()
{
	Super::PickedUp();
	
	AArcanumCloneCharacter* character = (AArcanumCloneCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	character->AddAmmunition(rewardAmmunition);
	SetActorEnableCollision(false);
	SetActorHiddenInGame(true);
}