// Mischa Ahi @ 2016

#pragma once

#include "PickUp.h"
#include "DestroyableItem.generated.h"

UCLASS()
class ARCANUMCLONE_API ADestroyableItem : public APickUp
{
	GENERATED_BODY()
	
public:

	ADestroyableItem();

	void BeginPlay() override;

	void PickedUp() override;

};