// Mischa Ahi / Philip Gandy @ 2016

#pragma once

#include "GearItem.h"
#include "ShieldItem.generated.h"

UCLASS()
class ARCANUMCLONE_API AShieldItem : public AGearItem
{
	GENERATED_BODY()
	
public:
	AShieldItem();

	void Use() override;

	void GiveStats() override;

	void RemoveStats() override;

protected:
	
	int armorvalue;

};